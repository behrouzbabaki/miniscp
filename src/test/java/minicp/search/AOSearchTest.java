package minicp.examples;

import minicp.cp.Factory;
import minicp.engine.core.Factor;
import minicp.engine.core.IntVar;
import minicp.engine.core.Solver;
import minicp.search.AndOrSearch;
import minicp.search.diagram.Node;
import minicp.util.VariableBranches;
import org.junit.Test;

import java.math.BigInteger;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class AOSearchTest {
    private double delta = 1E-14;

    class OrBoundsNoPruningTest {
        public AndOrSearch build() {
            Solver cp = Factory.makeSolver(false);

            int n = 2;
            IntVar[] x = new IntVar[n];

            for (int i = 0; i < n; i++) {
                x[i] = Factory.makeIntVar(cp, 0, 2, true, "x" + i);
            }

            AndOrSearch search = makeAO(cp, Factory.plus(x[0], 2021), new Factor[0], () -> {
                    for (int i = 0; i < x.length; i++) {
                        if (!x[i].isFixed())
                            return getBranches(x[i]);
                    }
                    return VariableBranches.NOBRANCH;
                });
            return search;
        }
    }

    @Test
    public void testOrBoundsNoPruning() {
        OrBoundsNoPruningTest t = new OrBoundsNoPruningTest();

        Node none = t.build().solve(false, false);
        Node caching = t.build().solve(true, false);
        Node bounding = t.build().solve(false, true);
        Node cachingBounding = t.build().solve(true, true);

        assertEquals(none.getValue(), 2023, delta);
        assertEquals(caching.getValue(), 2023, delta);
        assertEquals(bounding.getValue(), 2023, delta);
        assertEquals(cachingBounding.getValue(), 2023, delta);

        assertEquals(none.getSubgraphSize(), BigInteger.valueOf(12));
        assertEquals(caching.getSubgraphSize(), BigInteger.valueOf(12));
        assertEquals(bounding.getSubgraphSize(), BigInteger.valueOf(12));
        assertEquals(cachingBounding.getSubgraphSize(), BigInteger.valueOf(12));
    }

    class OrBoundsWithPruningTest {
        public AndOrSearch build() {
            Solver cp = Factory.makeSolver(false);

            int n = 2;
            IntVar[] x = new IntVar[n];

            for (int i = 0; i < n; i++) {
                x[i] = Factory.makeIntVar(cp, 0, 2, true, "x" + i);
            }

            AndOrSearch search = makeAO(cp, Factory.plus(Factory.minus(x[0]), 2023), new Factor[0], () -> {
                    for (int i = 0; i < x.length; i++) {
                        if (!x[i].isFixed())
                            return getBranches(x[i]);
                    }
                    return VariableBranches.NOBRANCH;
                });
            return search;
        }
    }

    @Test
    public void testOrBoundsWithPruning() {
        OrBoundsWithPruningTest t = new OrBoundsWithPruningTest();

        Node none = t.build().solve(false, false);
        Node caching = t.build().solve(true, false);
        Node bounding = t.build().solve(false, true);
        Node cachingBounding = t.build().solve(true, true);

        assertEquals(none.getValue(), 2023, delta);
        assertEquals(caching.getValue(), 2023, delta);
        assertEquals(bounding.getValue(), 2023, delta);
        assertEquals(cachingBounding.getValue(), 2023, delta);

        assertEquals(none.getSubgraphSize(), BigInteger.valueOf(12));
        assertEquals(caching.getSubgraphSize(), BigInteger.valueOf(12));
        assertEquals(bounding.getSubgraphSize(), BigInteger.valueOf(6));
        assertEquals(cachingBounding.getSubgraphSize(), BigInteger.valueOf(6));
    }


    public static AndOrSearch makeAO(Solver cp, IntVar objective, Factor[] factors,
                                     Supplier<VariableBranches> branching) {
        return new AndOrSearch(cp.getStateManager(), objective, factors, branching);
    }

    public static VariableBranches getBranches(IntVar x) {
        return new VariableBranches(x, IntStream.range(x.min(), x.max() + 1).filter(y -> x.contains(y)).toArray());
    }
}
