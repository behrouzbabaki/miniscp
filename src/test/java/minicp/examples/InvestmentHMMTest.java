package minicp.examples;

import minicp.search.diagram.Node;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.*;

public class InvestmentHMMTest {
    private double delta = 1E-14;

    @Test
    public void testNone() {
        InvestmentHMM model = new InvestmentHMM(3);
        Node root = model.search.solve(false, false);
        assertEquals(root.getValue(), 56.925108901754044, delta);
        assertEquals(model.search.getTreeSize(), 128038);
        assertEquals(root.getSubgraphSize(), BigInteger.valueOf(118150));
    }

    @Test
    public void testCaching() {
        InvestmentHMM model = new InvestmentHMM(3);
        Node root = model.search.solve(true, false);
        assertEquals(root.getValue(), 56.925108901754044, delta);
        assertEquals(model.search.getTreeSize(), 7462);
        assertEquals(root.getSubgraphSize(), BigInteger.valueOf(118150));
    }

    @Test
    public void testBounding() {
        InvestmentHMM model = new InvestmentHMM(3);
        Node root = model.search.solve(false, true);
        assertEquals(root.getValue(), 56.925108901754044, delta);
    }

    @Test
    public void testCachingBounding() {
        InvestmentHMM model = new InvestmentHMM(3);
        Node root = model.search.solve(true, true);
        assertEquals(root.getValue(), 56.925108901754044, delta);
    }
}
