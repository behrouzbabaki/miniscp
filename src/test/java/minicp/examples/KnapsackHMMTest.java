package minicp.examples;

import minicp.search.diagram.Node;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.*;

public class KnapsackHMMTest {
    private double delta = 1E-14;

    @Test
    public void testNone() {
        KnapsackHMM model = new KnapsackHMM(3);
        Node root = model.search.solve(false, false);
        assertEquals(root.getValue(), 33.856830661776776, delta);
        assertEquals(model.search.getTreeSize(), 374336);
        assertEquals(root.getSubgraphSize(), BigInteger.valueOf(374336));
    }

    @Test
    public void testCaching() {
        KnapsackHMM model = new KnapsackHMM(3);
        Node root = model.search.solve(true, false);
        assertEquals(root.getValue(), 33.856830661776776, delta);
        assertEquals(model.search.getTreeSize(), 8967);
        assertEquals(root.getSubgraphSize(), BigInteger.valueOf(374336));
    }

    @Test
    public void testBounding() {
        KnapsackHMM model = new KnapsackHMM(3);
        Node root = model.search.solve(false, true);
        assertEquals(root.getValue(), 33.856830661776776, delta);
    }

    @Test
    public void testCachingBounding() {
        KnapsackHMM model = new KnapsackHMM(3);
        Node root = model.search.solve(true, true);
        assertEquals(root.getValue(), 33.856830661776776, delta);
    }
}
