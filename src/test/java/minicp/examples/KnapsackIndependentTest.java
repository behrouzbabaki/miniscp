package minicp.examples;

import minicp.search.diagram.Node;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.*;

public class KnapsackIndependentTest {
    private double delta = 1E-14;

    @Test
    public void testNone() {
        KnapsackIndependent model = new KnapsackIndependent(3);
        Node root = model.search.solve(false, false);
        assertEquals(root.getValue(), 33.8865871912387, delta);
        assertEquals(model.search.getTreeSize(), 32246);
        assertEquals(root.getSubgraphSize(), BigInteger.valueOf(32246));
    }

    @Test
    public void testCaching() {
        KnapsackIndependent model = new KnapsackIndependent(3);
        Node root = model.search.solve(true, false);
        assertEquals(root.getValue(), 33.8865871912387, delta);
        assertEquals(model.search.getTreeSize(), 206);
        assertEquals(root.getSubgraphSize(), BigInteger.valueOf(32246));
    }

    @Test
    public void testBounding() {
        KnapsackIndependent model = new KnapsackIndependent(3);
        Node root = model.search.solve(false, true);
        assertEquals(root.getValue(), 33.8865871912387, delta);
    }

    @Test
    public void testCachingBounding() {
        KnapsackIndependent model = new KnapsackIndependent(3);
        Node root = model.search.solve(true, true);
        assertEquals(root.getValue(), 33.8865871912387, delta);
    }
}
