package minicp.examples;

import java.util.Arrays;
import java.util.HashSet;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import minicp.cp.Factory;
import minicp.engine.core.Factor;
import minicp.engine.core.IntVar;
import minicp.engine.core.Solver;
import minicp.search.AndOrSearch;
import minicp.search.diagram.Node;
import minicp.util.VariableBranches;

public class InvestmentChain {
    public AndOrSearch search;

    public InvestmentChain(int numberOfStages) {
        Solver cp = Factory.makeSolver(false);

        HashSet<Integer> optionAValues = new HashSet<>(Arrays.asList(10, 15, 20, 25));
        HashSet<Integer> optionBValues = new HashSet<>(Arrays.asList(20, 25, 30, 35));

        IntVar[] b = new IntVar[numberOfStages];
        IntVar[] a = new IntVar[numberOfStages];
        IntVar[] db = new IntVar[numberOfStages];
        IntVar[] da = new IntVar[numberOfStages];

        for (int t = 0; t < numberOfStages; t++) {
            a[t] = Factory.makeIntVar(cp, optionAValues, false, "opt_a" + t);
            b[t] = Factory.makeIntVar(cp, optionBValues, false, "opt_b" + t);
            da[t] = Factory.makeIntVar(cp, 0, 1, true, "decision_a" + t);
            db[t] = Factory.makeIntVar(cp, 0, 1, true, "decision_b" + t);
        }

        double[] b0Potentials = new double[] { 0.27586, 0.17784, 0.1837, 0.36260000000000003 };
        Factor b0Factor = new Factor(b[0], new IntVar[] {}, b0Potentials);
        cp.post(b0Factor);

        double[] a0Potentials = new double[] { 0.27586, 0.17784, 0.1837, 0.36260000000000003 };
        Factor a0Factor = new Factor(a[0], new IntVar[] {}, a0Potentials);
        cp.post(a0Factor);

        double[] aaPotentials = new double[] { 0.49655, 0.28384, 0.14708, 0.0725300000000001, 0.4403, 0.25682, 0.15642,
                                               0.14646000000000003, 0.22088, 0.15143, 0.19282, 0.43487, 0.05517, 0.07183, 0.22031, 0.65269 };
        Factor[] aaFactor = new Factor[numberOfStages - 1];
        for (int t = 0; t < numberOfStages - 1; t++) {
            aaFactor[t] = new Factor(a[t + 1], new IntVar[] { a[t] }, aaPotentials);
            cp.post(aaFactor[t]);
        }

        double[] bbPotentials = new double[] { 0.49655, 0.28384, 0.14708, 0.0725300000000001, 0.4403, 0.25682, 0.15642,
                                               0.14646000000000003, 0.22088, 0.15143, 0.19282, 0.43487, 0.05517, 0.07183, 0.22031, 0.65269 };
        Factor[] bbFactor = new Factor[numberOfStages - 1];
        for (int t = 0; t < numberOfStages - 1; t++) {
            bbFactor[t] = new Factor(b[t + 1], new IntVar[] { b[t] }, bbPotentials);
            cp.post(bbFactor[t]);
        }

        Factor[] factors = new Factor[2 * numberOfStages];
        int index = 0;
        factors[index++] = a0Factor;
        factors[index++] = b0Factor;
        for (int t = 0; t < numberOfStages - 1; t++) {
            factors[index++] = aaFactor[t];
            factors[index++] = bbFactor[t];
        }

        IntVar[] vars = new IntVar[numberOfStages * 4];
        index = 0;
        for (int t = 0; t < numberOfStages; t++) {
            vars[index++] = da[t];
            vars[index++] = db[t];
            vars[index++] = a[t];
            vars[index++] = b[t];
        }

        // add constraints
        // a nor b

        for (int t = 0; t < numberOfStages; t++) {
            cp.post(Factory.notEqual(da[t], db[t]));
        }

        // sum a > sum b

        int maximumValue = numberOfStages * 35;

        HashSet<Integer> tempAValues = new HashSet<>(optionAValues);
        tempAValues.add(0);
        HashSet<Integer> tempBValues = new HashSet<>(optionBValues);
        tempBValues.add(0);

        IntVar[] accumulatedA = new IntVar[numberOfStages];
        accumulatedA[0] = Factory.makeIntVar(cp, 0, maximumValue, true, "accum_a0");
        cp.post(Factory.mul(a[0], da[0], accumulatedA[0]));

        IntVar[] accumulatedB = new IntVar[numberOfStages];
        accumulatedB[0] = Factory.makeIntVar(cp, 0, maximumValue, true, "accum_b0");
        cp.post(Factory.mul(b[0], db[0], accumulatedB[0]));

        for (int t = 1; t < numberOfStages; t++) {
            IntVar aTemp = Factory.makeIntVar(cp, tempAValues, true, "a_temp" + t);
            cp.post(Factory.mul(a[t], da[t], aTemp));
            accumulatedA[t] = Factory.sum(accumulatedA[t - 1], aTemp);
        }

        for (int t = 1; t < numberOfStages; t++) {
            IntVar bTemp = Factory.makeIntVar(cp, tempBValues, true, "b_temp" + t);
            cp.post(Factory.mul(b[t], db[t], bTemp));
            accumulatedB[t] = Factory.sum(accumulatedB[t - 1], bTemp);
        }

        cp.post(Factory.lessOrEqual(accumulatedB[numberOfStages - 1], accumulatedA[numberOfStages - 1]));

        // add utility: sum b + sum a
        IntVar utility = Factory.sum(accumulatedB[numberOfStages - 1], accumulatedA[numberOfStages - 1]);

        this.search = makeAO(cp, utility, factors, () -> {
                for (int i = 0; i < vars.length; i++)
                    if (!vars[i].isFixed())
                        return getBranches(vars[i]);
                return VariableBranches.NOBRANCH;
            });
    }

    public static void main(String[] args) {

        if (args.length != 1) {
            System.err.println("argument [number of stages] is missing");
            System.exit(1);
        }
        int numberOfStages = Integer.parseInt(args[0]);

        InvestmentChain model = new InvestmentChain(numberOfStages);

        long startTime = System.nanoTime();
        Node root = model.search.solve(true);
        System.out.println("value: " + root.getValue());
        long endTime = System.nanoTime();
        double runtime = (endTime - startTime) / 1e9;
        System.out.println("runtime: " + runtime);

        System.out.println("tree size:" + model.search.getTreeSize());
        System.out.println("graph size:" + root.getSubgraphSize());

    }

    public static AndOrSearch makeAO(Solver cp, IntVar objective, Factor[] factors,
                                     Supplier<VariableBranches> branching) {
        return new AndOrSearch(cp.getStateManager(), objective, factors, branching);
    }

    public static VariableBranches getBranches(IntVar x) {
        return new VariableBranches(x, IntStream.range(x.min(), x.max() + 1).filter(y -> x.contains(y)).toArray());
    }

}
