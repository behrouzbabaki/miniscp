/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2018. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */

package minicp.examples;

import java.util.function.Supplier;
import java.util.stream.IntStream;

import minicp.cp.Factory;
import minicp.engine.core.Factor;
import minicp.engine.core.IntVar;
import minicp.engine.core.Solver;
import minicp.search.AndOrSearch;
import minicp.search.diagram.Node;
import minicp.util.VariableBranches;

public class Book {
    public static void main(String[] args) {

        Solver cp = Factory.makeSolver(false);


        IntVar v1 = Factory.makeIntVar(cp, 1, 2, true, "S1");
        IntVar v2 = Factory.makeIntVar(cp, 1, 2, true, "S2");
        IntVar s1 = Factory.makeIntVar(cp, 1, 2, false, "D1");
        IntVar s2 = Factory.makeIntVar(cp, 1, 2, false, "D2");
        IntVar h1 = Factory.makeIntVar(cp, 0, 1, false, "H1");
        IntVar h2 = Factory.makeIntVar(cp, 0, 1, false, "H2");

        IntVar l1 = Factory.makeIntVar(cp, 0, 2, true, "L1");
        cp.post(Factory.sum(new IntVar[] { v1, Factory.minus(s1) }, l1));

        IntVar l2 = Factory.makeIntVar(cp, 0, 3, true, "L2");
        cp.post(Factory.sum(new IntVar[] { l1, v2, Factory.minus(s2) }, l2));

        double[] h1Potentials = new double[] { 0.5, 0.5 };
        Factor h1Factor = new Factor(h1, new IntVar[] {}, h1Potentials);
        cp.post(h1Factor);

        double[] hhPotentials = new double[] { 0.9, 0.1, 0.2, 0.8 };
        Factor h2h1Factor = new Factor(h2, new IntVar[] { h1 }, hhPotentials);
        cp.post(h2h1Factor);

        double[] shPotentials = new double[] { 0.2, 0.8, 0.7, 0.3 };
        Factor s1h1Factor = new Factor(s1, new IntVar[] { h1 }, shPotentials);
        cp.post(s1h1Factor);
        Factor s2h2Factor = new Factor(s2, new IntVar[] { h2 }, shPotentials);
        cp.post(s2h2Factor);

        IntVar util = Factory.sum(l1, l2);

        Factor[] factors = new Factor[] { h1Factor, h2h1Factor, s1h1Factor, s2h2Factor };
        IntVar[] vars = new IntVar[] { v1, s1, v2, s2, h1, h2 };

        AndOrSearch search = makeAO(cp, Factory.minus(util), factors, () -> {
                for (int i = 0; i < vars.length; i++)
                    if (!vars[i].isFixed())
                        return getBranches(vars[i]);
                return VariableBranches.NOBRANCH;
            });

        Node searchTree = search.solve(false);
        searchTree.toDot("output/book_ao.gv");

        search = makeAO(cp, Factory.minus(util), factors, () -> {
                for (int i = 0; i < vars.length; i++)
                    if (!vars[i].isFixed())
                        return getBranches(vars[i]);
                return VariableBranches.NOBRANCH;
            });

        searchTree = search.solve(true);
        searchTree.toDot("output/book_aodd.gv");

    }

    public static AndOrSearch makeAO(Solver cp, IntVar objective, Factor[] factors,
                                     Supplier<VariableBranches> branching) {
        return new AndOrSearch(cp.getStateManager(), objective, factors, branching);
    }

    public static VariableBranches getBranches(IntVar x) {
        return new VariableBranches(x, IntStream.range(x.min(), x.max() + 1).filter(y -> x.contains(y)).toArray());
    }
}
