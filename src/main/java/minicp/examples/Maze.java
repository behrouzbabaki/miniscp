package minicp.examples;

import java.util.Arrays;
import java.util.HashSet;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import minicp.cp.Factory;
import minicp.engine.core.Factor;
import minicp.engine.core.IntVar;
import minicp.engine.core.Solver;
import minicp.search.AndOrSearch;
import minicp.util.VariableBranches;


public class Maze {
	
	
	public static class Location{
		
		int position_x;
		int position_y;
		
		public Location(int position_x, int position_y) {
			this.position_x = position_x;
			this.position_y = position_y;
		}
		
	}
	
	static int max_stage = 3;
	
	static int dimention_x = 9;
	static int dimention_y = 7;
	
	static Location goal = new Location(4,7);
	
	static Location[] free_spaces= new Location[23];
	

	public static void main(String[] args) {

		Solver cp = Factory.makeSolver(false);
		
		//Locations which are free spaces on the maze
		free_spaces[0] = new Location(1,2);
		free_spaces[1] = new Location(1,3);
		free_spaces[2] = new Location(1,4);
		free_spaces[3] = new Location(1,5);
		free_spaces[4] = new Location(1,6);
		free_spaces[5] = new Location(2,1);
		free_spaces[6] = new Location(2,2);
		free_spaces[7] = new Location(2,4);
		free_spaces[8] = new Location(2,6);
		free_spaces[9] = new Location(2,7);
		free_spaces[10] = new Location(3,2);
		free_spaces[11] = new Location(3,4);
		free_spaces[12] = new Location(3,6);
		free_spaces[13] = new Location(4,1);
		free_spaces[14] = new Location(4,2);
		free_spaces[15] = new Location(4,4);
		free_spaces[16] = new Location(4,6);
		free_spaces[17] = new Location(4,7);
		free_spaces[18] = new Location(5,2);
		free_spaces[19] = new Location(5,3);
		free_spaces[20] = new Location(5,4);
		free_spaces[21] = new Location(5,5);
		free_spaces[22] = new Location(5,6);

		IntVar[] x = new IntVar[max_stage];
		IntVar[] y = new IntVar[max_stage];
	
		IntVar[] ns = new IntVar[max_stage];
		IntVar[] es = new IntVar[max_stage];
		IntVar[] ss = new IntVar[max_stage];
		IntVar[] ws = new IntVar[max_stage];
		
		IntVar[] d = new IntVar[max_stage];
		
		HashSet<Integer> direction_values = new HashSet<>(Arrays.asList(0,1,2,3));
		//0: left, 1: up, 2:right, 3: down
		
		for(int t = 0; t< max_stage; t++) {
			//location variables for each stage
			x[t] = Factory.makeIntVar(cp, 0, dimention_x, false,"x");
			y[t] = Factory.makeIntVar(cp, 0, dimention_y, false,"y");
			//sensors at each stage
			ns[t] = Factory.makeIntVar(cp, 0, 1, false, "ns");
			es[t] = Factory.makeIntVar(cp, 0, 1, false, "es");
			ss[t] = Factory.makeIntVar(cp, 0, 1, false, "ss");
			ws[t] = Factory.makeIntVar(cp, 0, 1, false, "ws");
			//decision (action) variable for each stage to move
			d[t] = Factory.makeIntVar(cp, direction_values , true, "action");
		}
		
		
		double[] x0Potentials = LocationX0ProbabilityCalculator(free_spaces);
		Factor x0Factor = new Factor(x[0], new IntVar[] {}, x0Potentials);
		cp.post(x0Factor);
		
		double[] y0Potentials = new double[] {dimention_x*dimention_y};
		int index = 0;
		for (int i =0; i<dimention_x; i++) {
			double[] potential =  LocationY0ProbabilityCalculator(free_spaces, i);
			for(int j =0; j<potential.length; j++) {
				y0Potentials[index] = potential[j];
				index++;
			}
		}
		
		Factor y0Factor = new Factor(y[0], new IntVar[] {x[0]}, y0Potentials);
		cp.post(y0Factor);
		
		
		double[] xtPotentials = new double[] {direction_values.size()*dimention_y*dimention_x};
		index = 0;
		for(int action=0; action<direction_values.size(); action++) {
			for(int j=0; j<dimention_x; j++) {
				for(int k=0; k<dimention_y; k++) {
					double [] probs = LocationXProbabilityCalculator(action, new Location(j,k));
					for(int i=0; i<probs.length; i++) {
						xtPotentials[index] = probs[i];
						index++;
					}
				}
			}
		}
		
		
		Factor[] xtFactor = new Factor[max_stage] ;
		
		for(int t = 0; t< max_stage-1; t++) {
			xtFactor[t] = new Factor(x[t+1], new IntVar[] {x[t], y[t]}, xtPotentials);
			cp.post(xtFactor[t]);
		}
		
		double[] ytPotentials = new double[] {direction_values.size()*dimention_y*dimention_x*dimention_x};
		index = 0;
		for(int action=0; action<direction_values.size(); action++) {
			for(int j=0; j<dimention_x; j++) {
				for(int k=0; k<dimention_y; k++) {
					for(int l=0; l<dimention_x; l++) {
						double [] probs = LocationYProbabilityCalculator(action, new Location(j,k), l);
						for(int i=0; i<probs.length; i++) {
							ytPotentials[index] = probs[i];
							index++;
						}
					}
					
				}
			}
		}
		
		Factor[] ytFactor = new Factor[max_stage] ;
		for(int t = 0; t< max_stage-1; t++) {
			ytFactor[t] = new Factor(y[t+1], new IntVar[] {x[t], y[t], x[t+1]}, ytPotentials);
			cp.post(ytFactor[t]);
		}
		
	
		
		double[] nsPotentials = new double[] {dimention_y*dimention_x*2};
		index = 0;
		for (int i=0; i<dimention_x; i++) {
			for (int j=0; i<dimention_y; i++) {
				Location current = new Location(i,j);
				double[] prob= SensorProbabilityCalculator(1,current);
				nsPotentials[index]= prob[0];
				index++;
				nsPotentials[index]= prob[1];
				index++;
			}
		}
		
		double[] esPotentials = new double[] {dimention_y*dimention_x*2};
		index = 0;
		for (int i=0; i<dimention_x; i++) {
			for (int j=0; i<dimention_y; i++) {
				Location current = new Location(i,j);
				double[] prob= SensorProbabilityCalculator(2,current);
				nsPotentials[index]= prob[0];
				index++;
				nsPotentials[index]= prob[1];
				index++;
			}
		}
		double[] ssPotentials = new double[] {dimention_y*dimention_x*2};
		index = 0;
		for (int i=0; i<dimention_x; i++) {
			for (int j=0; i<dimention_y; i++) {
				Location current = new Location(i,j);
				double[] prob= SensorProbabilityCalculator(3,current);
				nsPotentials[index]= prob[0];
				index++;
				nsPotentials[index]= prob[1];
				index++;
			}
		}
		double[] wsPotentials = new double[] {dimention_y*dimention_x*2};
		index = 0;
		for (int i=0; i<dimention_x; i++) {
			for (int j=0; i<dimention_y; i++) {
				Location current = new Location(i,j);
				double[] prob= SensorProbabilityCalculator(0,current);
				nsPotentials[index]= prob[0];
				index++;
				nsPotentials[index]= prob[1];
				index++;
			}
		}
		
		
		Factor[] nsFactor = new Factor[max_stage] ;
		Factor[] esFactor = new Factor[max_stage] ;
		Factor[] ssFactor = new Factor[max_stage] ;
		Factor[] wsFactor = new Factor[max_stage] ;
		
		for(int t = 0; t< max_stage; t++) {
			nsFactor[t] = new Factor(d[t], new IntVar[] {x[t], y[t]}, nsPotentials);
			esFactor[t] = new Factor(d[t], new IntVar[] {x[t], y[t]}, esPotentials);
			ssFactor[t] = new Factor(d[t], new IntVar[] {x[t], y[t]}, ssPotentials);
			wsFactor[t] = new Factor(d[t], new IntVar[] {x[t], y[t]}, wsPotentials);
			cp.post(nsFactor[t]);
			cp.post(esFactor[t]);
			cp.post(ssFactor[t]);
			cp.post(wsFactor[t]);
	
		}
		
		//this is the relaxed version
		//TODO: do we really need this one?
		double[] dPotentials = new double[] {};
		Factor[] dFactor = new Factor[max_stage] ;
		
		for(int t = 0; t< max_stage; t++) {
			dFactor[t] = new Factor(d[t], new IntVar[] {ns[t], es[t], ss[t], ws[t]}, dPotentials);
			cp.post(dFactor[t]);
	
		}
		
		
		Factor[] factors =  new Factor[7*max_stage];
		index = 0;
		factors[index] = x0Factor;
		index++;
		factors[index] = y0Factor;
		index++;
		for(int t = 0; t< max_stage-1; t++) {
			factors[index]= xtFactor[t];
			index++;
			factors[index]= ytFactor[t];
			index++;
		}
		for(int t = 0; t< max_stage; t++) {
			factors[index]= nsFactor[t];
			index++;
			factors[index]= esFactor[t];
			index++;
			factors[index]= ssFactor[t];
			index++;
			factors[index]= wsFactor[t];
			index++;
			factors[index]= dFactor[t];
			index++;
		}
		
		IntVar[] vars = new IntVar[max_stage*7];
		index = 0;
		for(int t=0;t<max_stage;t++) {
			vars[index] =x[t];
			index++;
			vars[index] = y[t];
			index++;
			vars[index] = d[t];
			index++;
			vars[index] = ns[t];
			index++;
			vars[index] = es[t];
			index++;
			vars[index] = ss[t];
			index++;
			vars[index] = ws[t];
			index++;
		}
				
		//add utility
		IntVar utility = Factory.makeIntVar(cp, 0, max_stage, false, "utility"); 
		utility = Factory.plus(Factory.minus(utility), max_stage);
		
		AndOrSearch search = makeAO(cp, Factory.maximum(utility), factors , () -> {
			for (int i = 0; i < vars.length; i++)
				if (!vars[i].isFixed())
					return getBranches(vars[i]);
				return VariableBranches.NOBRANCH;
			});

			search.solve();

		}

		public static AndOrSearch makeAO(Solver cp, IntVar objective, Factor[] factors,
				Supplier<VariableBranches> branching) {
			return new AndOrSearch(cp.getStateManager(), objective, factors, branching);
		}

		public static VariableBranches getBranches(IntVar x) {
			return new VariableBranches(x, IntStream.range(x.min(), x.max() + 1).filter(y -> x.contains(y)).toArray());
		}

		
		public static double[] SensorProbabilityCalculator(int sensor, Location current) {
			//P(sensor|x,y)
			//sensor info: 0: left, 1: up, 2:right, 3: down
			double[] probs = new double[2];
			
			//which Location to move, and probability of moving to the location
			Location right = new Location(current.position_x, current.position_y+1) ;
			Location left = new Location(current.position_x, current.position_y-1) ;
			Location up = new Location(current.position_x-1, current.position_y) ;
			Location down = new Location(current.position_x+1, current.position_y) ;
			
			if (sensor ==0) {
				if (Arrays.asList(free_spaces).contains(left)){
					probs[0] = 0.05;
					probs[1] = 0.95;
				}
				else {
					probs[0] = 0.90;
					probs[1] = 0.10;
				}
			}
			
			if (sensor ==1) {
				if (Arrays.asList(free_spaces).contains(up)){
					probs[0] = 0.05;
					probs[1] = 0.95;
				}
				else {
					probs[0] = 0.90;
					probs[1] = 0.10;	
				}
			}
			
			if (sensor ==2) {
				if (Arrays.asList(free_spaces).contains(right)){
					probs[0] = 0.05;
					probs[1] = 0.95;
				}
				else {
					probs[0] = 0.90;
					probs[1] = 0.10;
				}
			}
			
			if (sensor ==3) {
				if (Arrays.asList(free_spaces).contains(down)){
					probs[0] = 0.05;
					probs[1] = 0.95;
				}
				else {
					probs[0] = 0.90;
					probs[1] = 0.10;
				}
			}
			return probs;
			
		}
		
		//TODO: this needs to be checked!
		public static double[] LocationYProbabilityCalculator(int action, Location current, int position_x) {
			//p(yt+1|xt+1,yt,xt,d)
			//action info: 0: left, 1: up, 2:right, 3: down
			double[] final_probs = new double[dimention_y];
			double[] probs = new double[dimention_x*dimention_y];
			boolean[] next_position = new boolean[4];
			
			//which Location to move, and probability of moving to the location
			Location right = new Location(current.position_x, current.position_y+1) ;
			Location left = new Location(current.position_x, current.position_y-1) ;
			Location up = new Location(current.position_x-1, current.position_y) ;
			Location down = new Location(current.position_x+1, current.position_y) ;
			
			if (Arrays.asList(free_spaces).contains(left)) next_position[0]=true; else next_position[0]=false;
			if (Arrays.asList(free_spaces).contains(up)) next_position[1]=true; else next_position[1]=false;
			if (Arrays.asList(free_spaces).contains(right)) next_position[2]=true; else next_position[2]=false;
			if (Arrays.asList(free_spaces).contains(down)) next_position[3]=true; else next_position[3]=false;
			
			if (action ==0) {
				probs[get_index(current)] = 0.089;
				if (next_position[0]) probs[get_index(left)] = 0.89;
				else probs[get_index(current)] +=0.89;
				if (next_position[2]) probs[get_index(right)] = 0.001;
				else probs[get_index(current)] +=0.001;
				if (next_position[1]) probs[get_index(up)] = 0.01;
				else probs[get_index(current)] +=0.01;
				if (next_position[3]) probs[get_index(down)] = 0.01;
				else probs[get_index(current)] +=0.01;	
			}
			if (action ==1) {
				probs[get_index(current)] = 0.089;
				if(next_position[1]) probs[get_index(up)] = 0.89;
				else probs[get_index(current)] +=0.89;
				if(next_position[3]) probs[get_index(down)] = 0.001;
				else probs[get_index(current)] +=0.001;	
				if(next_position[0]) probs[get_index(left)] = 0.01;
				else probs[get_index(current)] +=0.01;	
				if(next_position[2]) probs[get_index(right)] = 0.01;
				else probs[get_index(current)] +=0.01;			
				}	
			if (action ==2) {
				probs[get_index(current)] = 0.089;
				if(next_position[2]) probs[get_index(right)] = 0.89;
				else probs[get_index(current)] +=0.89;	
				if(next_position[0]) probs[get_index(left)] = 0.001;
				else probs[get_index(current)] +=0.001;	
				if(next_position[1]) probs[get_index(up)] = 0.01;
				else probs[get_index(current)] +=0.01;	
				if(next_position[3]) probs[get_index(down)] = 0.01;
				else probs[get_index(current)] +=0.01;	
			}if (action ==3) {
				probs[get_index(current)] = 0.089;
				if(next_position[3]) probs[get_index(down)] = 0.89;
				else probs[get_index(current)] +=0.89;
				if(next_position[1]) probs[get_index(up)] = 0.001;
				else probs[get_index(current)] +=0.001;	
				if(next_position[0]) probs[get_index(left)] = 0.01;
				else probs[get_index(current)] +=0.01;	
				if(next_position[2]) probs[get_index(right)] = 0.01;
				else probs[get_index(current)] +=0.01;	
			}
			
			for(int i = 0; i<free_spaces.length; i++) {
				final_probs[free_spaces[i].position_y] += probs[get_index(free_spaces[i])];
			}
			return probs;
			
		}
		
		public static int get_index(Location location) {
			return location.position_x*(dimention_x)+ location.position_y;
		}
		
		//TODO: this needs to be checked!
		public static double[] LocationXProbabilityCalculator(int action, Location current) {
			//p(xt+1|xt,yt,d)
			//action info: 0: left, 1: up, 2:right, 3: down
			double[] final_probs = new double[dimention_x];
			double[] probs = new double[dimention_x*dimention_y];
			boolean[] next_position = new boolean[4];
			
			//which Location to move, and probability of moving to the location
			Location right = new Location(current.position_x, current.position_y+1) ;
			Location left = new Location(current.position_x, current.position_y-1) ;
			Location up = new Location(current.position_x-1, current.position_y) ;
			Location down = new Location(current.position_x+1, current.position_y) ;
			
			if (Arrays.asList(free_spaces).contains(left)) next_position[0]=true; else next_position[0]=false;
			if (Arrays.asList(free_spaces).contains(up)) next_position[1]=true; else next_position[1]=false;
			if (Arrays.asList(free_spaces).contains(right)) next_position[2]=true; else next_position[2]=false;
			if (Arrays.asList(free_spaces).contains(down)) next_position[3]=true; else next_position[3]=false;
			
			if (action ==0) {
				probs[get_index(current)] = 0.089;
				if (next_position[0]) probs[get_index(left)] = 0.89;
				else probs[get_index(current)] +=0.89;
				if (next_position[2]) probs[get_index(right)] = 0.001;
				else probs[get_index(current)] +=0.001;
				if (next_position[1]) probs[get_index(up)] = 0.01;
				else probs[get_index(current)] +=0.01;
				if (next_position[3]) probs[get_index(down)] = 0.01;
				else probs[get_index(current)] +=0.01;	
			}
			if (action ==1) {
				probs[get_index(current)] = 0.089;
				if(next_position[1]) probs[get_index(up)] = 0.89;
				else probs[get_index(current)] +=0.89;
				if(next_position[3]) probs[get_index(down)] = 0.001;
				else probs[get_index(current)] +=0.001;	
				if(next_position[0]) probs[get_index(left)] = 0.01;
				else probs[get_index(current)] +=0.01;	
				if(next_position[2]) probs[get_index(right)] = 0.01;
				else probs[get_index(current)] +=0.01;			
				}	
			if (action ==2) {
				probs[get_index(current)] = 0.089;
				if(next_position[2]) probs[get_index(right)] = 0.89;
				else probs[get_index(current)] +=0.89;	
				if(next_position[0]) probs[get_index(left)] = 0.001;
				else probs[get_index(current)] +=0.001;	
				if(next_position[1]) probs[get_index(up)] = 0.01;
				else probs[get_index(current)] +=0.01;	
				if(next_position[3]) probs[get_index(down)] = 0.01;
				else probs[get_index(current)] +=0.01;	
			}if (action ==3) {
				probs[get_index(current)] = 0.089;
				if(next_position[3]) probs[get_index(down)] = 0.89;
				else probs[get_index(current)] +=0.89;
				if(next_position[1]) probs[get_index(up)] = 0.001;
				else probs[get_index(current)] +=0.001;	
				if(next_position[0]) probs[get_index(left)] = 0.01;
				else probs[get_index(current)] +=0.01;	
				if(next_position[2]) probs[get_index(right)] = 0.01;
				else probs[get_index(current)] +=0.01;	
			}
			
			for(int i = 0; i<free_spaces.length; i++) {
				final_probs[free_spaces[i].position_x] += probs[get_index(free_spaces[i])];
			}
			return final_probs;
		}
		

		public static double[] LocationX0ProbabilityCalculator(Location[] free_spaces) {
			//p(x0)
			//action info: 0: left, 1: up, 2:right, 3: down
			double[] probs = new double[dimention_x];
			double size = free_spaces.length;
			double equal_prob = 100/size;
			
			for(int i = 0; i<size; i++) {
				probs[free_spaces[i].position_x] += equal_prob;
			}
			
			return probs;
		}
		
		public static double[] LocationY0ProbabilityCalculator(Location[] free_spaces, int position_x) {
			//p(y0|x0)
			double[] probs = new double[dimention_y];
			double number = 0;
			for(int i = 0; i<free_spaces.length; i++) {
				if(free_spaces[i].position_x == position_x) {
					probs[free_spaces[i].position_y]=1;
					number+=1;
				}	
			}
			for (int i= 0; i<dimention_y; i++ ) {
				probs[i]*= 100/number;
			}
			
			return probs;
		}

}
