package minicp.examples;

import java.util.Arrays;
import java.util.HashSet;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import minicp.cp.Factory;
import minicp.engine.core.Factor;
import minicp.engine.core.IntVar;
import minicp.engine.core.Solver;
import minicp.search.AndOrSearch;
import minicp.search.diagram.Node;
import minicp.util.VariableBranches;

public class KnapsackHMM {
    public AndOrSearch search;

    public KnapsackHMM(int numberOfStages) {
        Solver cp = Factory.makeSolver(false);

        HashSet<Integer> hidden_values = new HashSet<>(Arrays.asList(1, 2));
        HashSet<Integer> weight_values = new HashSet<>(Arrays.asList(15, 20, 25, 30, 35));
        HashSet<Integer> reward_values = new HashSet<>(Arrays.asList(10, 15, 20));

        IntVar[] h = new IntVar[numberOfStages];
        IntVar[] w = new IntVar[numberOfStages];
        IntVar[] r = new IntVar[numberOfStages];
        IntVar[] d = new IntVar[numberOfStages];
        for (int t = 0; t < numberOfStages; t++) {
            // hidden variable for each item
            h[t] = Factory.makeIntVar(cp, hidden_values, false, "hidden");
            // weight of an item
            w[t] = Factory.makeIntVar(cp, weight_values, false, "weight");
            // value of an item
            r[t] = Factory.makeIntVar(cp, reward_values, false, "value");
            // decision of taking an item
            d[t] = Factory.makeIntVar(cp, 0, 1, true, "decision");
        }

        double[] h0Potentials = new double[] { 0.3666666666666667, 0.6333333333333333 };
        Factor h0Factor = new Factor(h[0], new IntVar[] {}, h0Potentials);
        cp.post(h0Factor);

        double[] hhPotentials = new double[] { 0.6333333333333333, 0.3666666666666667, 0.3666666666666667,
                                               0.6333333333333333 };
        Factor[] hhFactor = new Factor[numberOfStages - 1];
        for (int t = 0; t < numberOfStages - 1; t++) {
            hhFactor[t] = new Factor(h[t + 1], new IntVar[] { h[t] }, hhPotentials);
            cp.post(hhFactor[t]);

        }

        double[] hrPotentials = new double[] { 0.4177376677004119, 0.3410813774021089, 0.24118095489747926,
                                               0.16666666666666666, 0.3333333333333333, 0.5 };
        Factor[] hrFactor = new Factor[numberOfStages];

        for (int t = 0; t < numberOfStages; t++) {
            hrFactor[t] = new Factor(r[t], new IntVar[] { h[t] }, hrPotentials);
            cp.post(hrFactor[t]);

        }

        double[] hwPotentials = new double[] { 0.26675964216358283, 0.23859707741251612, 0.20663113030796126,
                                               0.16871361140968177, 0.11929853870625809, 0.06666666666666668, 0.13333333333333333, 0.2,
                                               0.26666666666666666, 0.3333333333333333 };
        Factor[] hwFactor = new Factor[numberOfStages];

        for (int t = 0; t < numberOfStages; t++) {
            hwFactor[t] = new Factor(w[t], new IntVar[] { h[t] }, hwPotentials);
            cp.post(hwFactor[t]);

        }

        Factor[] factors = new Factor[3 * numberOfStages];
        int index = 0;
        factors[index] = h0Factor;
        index++;
        for (int t = 0; t < numberOfStages - 1; t++) {
            factors[index] = hhFactor[t];
            index++;
        }
        for (int t = 0; t < numberOfStages; t++) {
            factors[index] = hrFactor[t];
            index++;
            factors[index] = hwFactor[t];
            index++;
        }

        IntVar[] vars = new IntVar[numberOfStages * 4];
        index = 0;
        for (int t = 0; t < numberOfStages; t++) {
            vars[index] = d[t];
            index++;
            vars[index] = r[t];
            index++;
            vars[index] = w[t];
            index++;
        }
        for (int t = 0; t < numberOfStages; t++) {
            vars[index] = h[t];
            index++;
        }

        // add constraints
        int capacity = numberOfStages * 25;
        IntVar weighted_items = Factory.makeIntVar(cp, 0, capacity, true, "weighted_items");
        cp.post(Factory.mul(w, d, weighted_items));

        // add utility
        int maximum_value = numberOfStages * 20;
        IntVar utility = Factory.makeIntVar(cp, 0, maximum_value, true, "utility");
        cp.post(Factory.mul(d, r, utility));

        this.search = makeAO(cp, utility, factors, () -> {
                for (int i = 0; i < vars.length; i++)
                    if (!vars[i].isFixed())
                        return getBranches(vars[i]);
                return VariableBranches.NOBRANCH;
            });
    }

    public static void main(String[] args) {

        if (args.length != 1) {
            System.err.println("argument [number of stages] is missing");
            System.exit(1);
        }
        int numberOfStages = Integer.parseInt(args[0]);

        KnapsackHMM model = new KnapsackHMM(numberOfStages);

        long startTime = System.nanoTime();
        Node root = model.search.solve(true);
        System.out.println("value: " + root.getValue());
        long endTime = System.nanoTime();
        double runtime = (endTime - startTime) / 1e9;
        System.out.println("runtime: " + runtime);

        System.out.println("tree size:" + model.search.getTreeSize());
        System.out.println("graph size:" + root.getSubgraphSize());

    }

    public static AndOrSearch makeAO(Solver cp, IntVar objective, Factor[] factors,
                                     Supplier<VariableBranches> branching) {
        return new AndOrSearch(cp.getStateManager(), objective, factors, branching);
    }

    public static VariableBranches getBranches(IntVar x) {
        return new VariableBranches(x, IntStream.range(x.min(), x.max() + 1).filter(y -> x.contains(y)).toArray());
    }

}
