package minicp.examples;

import java.util.Arrays;
import java.util.HashSet;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import minicp.cp.Factory;
import minicp.engine.core.Factor;
import minicp.engine.core.IntVar;
import minicp.engine.core.Solver;
import minicp.search.AndOrSearch;
import minicp.search.diagram.Node;
import minicp.util.VariableBranches;

public class KnapsackChain {
    public AndOrSearch search;

    public KnapsackChain(int numberOfStages) {
        Solver cp = Factory.makeSolver(false);

        HashSet<Integer> weight_values = new HashSet<>(Arrays.asList(15, 20, 25, 30, 35));
        HashSet<Integer> temp_weight_values = new HashSet<>(Arrays.asList(0, 15, 20, 25, 30, 35));
        HashSet<Integer> reward_values = new HashSet<>(Arrays.asList(10, 15, 20));

        IntVar[] w = new IntVar[numberOfStages];
        IntVar[] r = new IntVar[numberOfStages];
        IntVar[] d = new IntVar[numberOfStages];
        for (int t = 0; t < numberOfStages; t++) {
            // weight of an item
            w[t] = Factory.makeIntVar(cp, weight_values, false, "weight");
            // value of an item
            r[t] = Factory.makeIntVar(cp, reward_values, false, "value");
            // decision of taking an item
            d[t] = Factory.makeIntVar(cp, 0, 1, true, "decision");
        }

        double[] r0Potentials = new double[] { 0.2587, 0.3362, 0.4051};
        Factor r0Factor = new Factor(r[0], new IntVar[] {}, r0Potentials);
        cp.post(r0Factor);

        double[] w0Potentials = new double[] { 0.14, 0.1719, 0.2024, 0.2308, 0.2549 };
        Factor w0Factor = new Factor(w[0], new IntVar[] {}, w0Potentials);
        cp.post(w0Factor);

        double[] wwPotentials = new double[] { 0.1773, 0.1915, 0.2037, 0.2125, 0.2150, 0.1672, 0.1862, 0.2033, 0.2175, 0.2258, 0.1600, 0.1824, 0.2031, 0.2210, 0.2335, 0.1543, 0.1795, 0.2029, 0.2237, 0.2396, 0.14920000000000003, 0.17670000000000002, 0.20270000000000002,  0.22630000000000003, 0.24510000000000004 };
        Factor[] wwFactor = new Factor[numberOfStages - 1];
        for (int t = 0; t < numberOfStages - 1; t++) {
            wwFactor[t] = new Factor(w[t + 1], new IntVar[] { w[t] }, wwPotentials);
            cp.post(wwFactor[t]);
        }

        double[] rrPotentials = new double[] { 0.2984, 0.3374, 0.3642, 0.2836, 0.337, 0.3794, 0.2733, 0.3367, 0.39 };
        Factor[] rrFactor = new Factor[numberOfStages - 1];
        for (int t = 0; t < numberOfStages - 1; t++) {
            rrFactor[t] = new Factor(r[t + 1], new IntVar[] {r[t]}, rrPotentials);
            cp.post(rrFactor[t]);
        }

        Factor[] factors = new Factor[2 * numberOfStages];
        int index = 0;
        factors[index] = r0Factor;
        index++;
        factors[index] = w0Factor;
        index++;
        for (int t = 0; t < numberOfStages - 1; t++) {
            factors[index] = rrFactor[t];
            index++;
            factors[index] = wwFactor[t];
            index++;
        }

        IntVar[] vars = new IntVar[numberOfStages * 3];
        index = 0;
        for (int t = 0; t < numberOfStages; t++) {
            vars[index] = d[t];
            index++;
            vars[index] = r[t];
            index++;
            vars[index] = w[t];
            index++;
        }

        // add constraints
        int capacity = numberOfStages * 25;
        IntVar[] accumaltedWeights = new IntVar[numberOfStages];

        accumaltedWeights[0] = Factory.makeIntVar(cp, 0, capacity, true, "accum_weight");
        cp.post(Factory.mul(new IntVar[] {w[0]}, new IntVar[] {d[0]}, accumaltedWeights[0]));

        for (int t = 0; t < numberOfStages-1; t++) {
            IntVar w_temp = Factory.makeIntVar(cp, temp_weight_values, true, "weight_temp");
            cp.post(Factory.mul(new IntVar[] {w[t+1]}, new IntVar[] {d[t+1]}, w_temp));
            accumaltedWeights[t+1] = Factory.makeIntVar(cp, 0, capacity, true, "accum_weight");
            cp.post(Factory.sum(new IntVar[] {accumaltedWeights[t],w_temp} , accumaltedWeights[t+1]));
        }


        // add utility
        int maximum_value = numberOfStages * 20;
        //IntVar utility = Factory.makeIntVar(cp, 0, maximum_value, true, "utility");
        //cp.post(Factory.mul(d, r, utility));

        IntVar[] accumaltedUtility = new IntVar[numberOfStages];

        accumaltedUtility[0] = Factory.makeIntVar(cp, 0, maximum_value, true, "accum_utility");
        cp.post(Factory.mul(new IntVar[] {r[0]}, new IntVar[] {d[0]}, accumaltedUtility[0]));

        for (int t = 0; t < numberOfStages-1; t++) {
            IntVar u_temp = Factory.makeIntVar(cp, 0, maximum_value, true, "utility_temp");
            cp.post(Factory.mul(new IntVar[] {r[t+1]}, new IntVar[] {d[t+1]}, u_temp));
            accumaltedUtility[t+1] = Factory.makeIntVar(cp, 0, capacity, true, "accum_utility");
            cp.post(Factory.sum(new IntVar[] {accumaltedUtility[t],u_temp} , accumaltedUtility[t+1]));
        }

        this.search = makeAO(cp, accumaltedUtility[numberOfStages-1], factors, () -> {
                for (int i = 0; i < vars.length; i++)
                    if (!vars[i].isFixed())
                        return getBranches(vars[i]);
                return VariableBranches.NOBRANCH;
            });
    }

    public static void main(String[] args) {

        if (args.length != 1) {
            System.err.println("argument [number of stages] is missing");
            System.exit(1);
        }
        int numberOfStages = Integer.parseInt(args[0]);

        KnapsackChain model = new KnapsackChain(numberOfStages);

        long startTime = System.nanoTime();
        Node root  = model.search.solve(true);
        System.out.println("value: " + root.getValue());
        long endTime = System.nanoTime();
        double runtime = (endTime - startTime) / 1e9;
        System.out.println("runtime: " + runtime);

        System.out.println("tree size:" + model.search.getTreeSize());
        System.out.println("graph size:" + root.getSubgraphSize());

    }

    public static AndOrSearch makeAO(Solver cp, IntVar objective, Factor[] factors,
                                     Supplier<VariableBranches> branching) {
        return new AndOrSearch(cp.getStateManager(), objective, factors, branching);
    }

    public static VariableBranches getBranches(IntVar x) {
        return new VariableBranches(x, IntStream.range(x.min(), x.max() + 1).filter(y -> x.contains(y)).toArray());
    }

}
