/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2018. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */

package minicp.examples;

import java.util.function.Supplier;
import java.util.stream.IntStream;

import minicp.cp.Factory;
import minicp.engine.core.Factor;
import minicp.engine.core.IntVar;
import minicp.engine.core.Solver;
import minicp.search.AndOrSearch;
import minicp.search.diagram.Node;
import minicp.util.VariableBranches;

public class ToySCP {
    public static void main(String[] args) {

        Solver cp = Factory.makeSolver(false);

        IntVar x1 = Factory.makeIntVar(cp, 2, true, "x1");
        IntVar x2 = Factory.makeIntVar(cp, 2, true, "x2");

        IntVar s1 = Factory.makeIntVar(cp, 2, false, "s1");
        IntVar s2 = Factory.makeIntVar(cp, 2, false, "s2");

        double[] fPotentials = new double[] { 0.7, 0.3, 0.65, 0.35 };
        Factor f = new Factor(s2, new IntVar[] { s1 }, fPotentials);
        cp.post(f);

        AndOrSearch search = makeAO(cp, x2, new Factor[] { f }, () -> {
                IntVar[] vars = new IntVar[] { x1, s1, x2, s2 };
                for (int i = 0; i < vars.length; i++)
                    if (!vars[i].isFixed())
                        return getBranches(vars[i]);
                return VariableBranches.NOBRANCH;
            });

        Node root = search.solve();
        root.toDot("output/toy.gv");

    }

    public static AndOrSearch makeAO(Solver cp, IntVar objective, Factor[] factors,
                                     Supplier<VariableBranches> branching) {
        return new AndOrSearch(cp.getStateManager(), objective, factors, branching);
    }

    public static VariableBranches getBranches(IntVar x) {
        return new VariableBranches(x, IntStream.range(x.min(), x.max() + 1).filter(y -> x.contains(y)).toArray());
    }
}
