package minicp.examples;

import java.util.Arrays;
import java.util.HashSet;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import minicp.cp.Factory;
import minicp.engine.core.Factor;
import minicp.engine.core.IntVar;
import minicp.engine.core.Solver;
import minicp.search.AndOrSearch;
import minicp.search.diagram.Node;
import minicp.util.VariableBranches;

public class InvestmentHMM {
    public AndOrSearch search;

    public InvestmentHMM(int numberOfStages) {
        Solver cp = Factory.makeSolver(false);

        HashSet<Integer> option_A_values = new HashSet<>(Arrays.asList(10, 15, 20, 25));
        HashSet<Integer> option_B_values = new HashSet<>(Arrays.asList(20, 25, 30, 35));

        HashSet<Integer> temp_A_values = new HashSet<>(Arrays.asList(0, 10, 15, 20, 25));
        HashSet<Integer> temp_B_values = new HashSet<>(Arrays.asList(0, 20, 25, 30, 35));

        IntVar[] b = new IntVar[numberOfStages];
        IntVar[] a = new IntVar[numberOfStages];
        IntVar[] h = new IntVar[numberOfStages];
        IntVar[] db = new IntVar[numberOfStages];
        IntVar[] da = new IntVar[numberOfStages];

        for (int t = 0; t < numberOfStages; t++) {
            a[t] = Factory.makeIntVar(cp, option_A_values, false, "opt_a");
            b[t] = Factory.makeIntVar(cp, option_B_values, false, "opt_b");
            h[t] = Factory.makeIntVar(cp, 0, 1, false, "hidden");
            da[t] = Factory.makeIntVar(cp, 0, 1, true, "decision_a");
            db[t] = Factory.makeIntVar(cp, 0, 1, true, "decision_b");
        }


        double[] h0Potentials = new double[] {0.5, 0.5};
        Factor h0Factor = new Factor(h[0], new IntVar[] {}, h0Potentials);
        cp.post(h0Factor);


        double[] hhPotentials = new double[] {0.9, 0.1, 0.1, 0.9};
        Factor[] hhFactor = new Factor[numberOfStages - 1];
        for (int t = 0; t < numberOfStages - 1; t++) {
            hhFactor[t] = new Factor(h[t + 1], new IntVar[] {h[t]}, hhPotentials);
            cp.post(hhFactor[t]);
        }

        double[] ahPotentials = new double[] {0.0, 0.045325779036827184, 0.2294617563739378, 0.7252124645892349, 0.5517241379310345, 0.3103448275862069, 0.13793103448275856, 0.0};
        Factor[] ahFactor = new Factor[numberOfStages];
        for (int t = 0; t < numberOfStages ; t++) {
            ahFactor[t] = new Factor(a[t], new IntVar[] {h[t]}, ahPotentials);
            cp.post(ahFactor[t]);
        }

        double[] bhPotentials = new double[] {0.5517241379310345, 0.3103448275862069, 0.13793103448275856, 0.0, 0.0, 0.045325779036827184, 0.2294617563739378, 0.7252124645892349};
        Factor[] bhFactor = new Factor[numberOfStages ];
        for (int t = 0; t < numberOfStages; t++) {
            bhFactor[t] = new Factor(b[t], new IntVar[] {h[t]}, bhPotentials);
            cp.post(bhFactor[t]);
        }

        Factor[] factors = new Factor[3 * numberOfStages];
        int index = 0;
        factors[index] = h0Factor;
        index++;
        for (int t = 0; t < numberOfStages-1 ; t++) {
            factors[index] = hhFactor[t];
            index++;
        }
        for (int t = 0; t < numberOfStages; t++) {
            factors[index] = ahFactor[t];
            index++;
            factors[index] = bhFactor[t];
            index++;
        }


        IntVar[] vars = new IntVar[numberOfStages * 5];
        index = 0;
        for (int t = 0; t < numberOfStages; t++) {
            vars[index] = da[t];
            index++;
            vars[index] = db[t];
            index++;
            vars[index] = a[t];
            index++;
            vars[index] = b[t];
            index++;
        }
        for (int t = 0; t < numberOfStages; t++) {
            vars[index] = h[t];
            index++;
        }

        // add constraints
        // a nor b

        for (int t = 0; t < numberOfStages; t++) {
            cp.post(Factory.notEqual(da[t], db[t]));
        }


        // sum b < sum a

        int maximum_value = numberOfStages * 35;


        IntVar[] accumalteda = new IntVar[numberOfStages];
        accumalteda[0] = Factory.makeIntVar(cp, 0, maximum_value, true, "accum_a0");
        cp.post(Factory.mul(new IntVar[] {a[0]}, new IntVar[] {da[0]}, accumalteda[0]));

        IntVar[] accumaltedb = new IntVar[numberOfStages];
        accumaltedb[0] = Factory.makeIntVar(cp, 0, maximum_value, true, "accum_b0");
        cp.post(Factory.mul(new IntVar[] {b[0]}, new IntVar[] {db[0]}, accumaltedb[0]));

        for (int t = 0; t < numberOfStages-1; t++) {
            IntVar a_temp = Factory.makeIntVar(cp, temp_A_values , true, "a_temp"+(t+1));
            cp.post(Factory.mul(new IntVar[] {a[t+1]}, new IntVar[] {da[t+1]}, a_temp));
            accumalteda[t+1] = Factory.makeIntVar(cp, 0, maximum_value, true, "accum_a"+(t+1));
            cp.post(Factory.sum(new IntVar[] {accumalteda[t],a_temp} , accumalteda[t+1]));
        }

        for (int t = 0; t < numberOfStages-1; t++) {
            IntVar b_temp = Factory.makeIntVar(cp, temp_B_values , true, "b_temp"+(t+1));
            cp.post(Factory.mul(new IntVar[] {b[t+1]}, new IntVar[] {db[t+1]}, b_temp));
            accumaltedb[t+1] = Factory.makeIntVar(cp, 0, maximum_value, true, "accum_b"+(t+1));
            cp.post(Factory.sum(new IntVar[] {accumaltedb[t],b_temp} , accumaltedb[t+1]));
        }

        cp.post(Factory.lessOrEqual(accumaltedb[numberOfStages-1],accumalteda[numberOfStages-1]));


        // add utility: sum b + sum a

        IntVar utility = Factory.makeIntVar(cp, 0, 2* maximum_value , true, "utility");
        cp.post(Factory.sum(new IntVar[] {accumaltedb[numberOfStages-1], accumalteda[numberOfStages-1]}, utility));

        this.search = makeAO(cp, utility, factors, () -> {
                for (int i = 0; i < vars.length; i++)
                    if (!vars[i].isFixed())
                        return getBranches(vars[i]);
                return VariableBranches.NOBRANCH;
            });
    }

    public static void main(String[] args) {

        if (args.length != 1) {
            System.err.println("argument [number of stages] is missing");
            System.exit(1);
        }
        int numberOfStages = Integer.parseInt(args[0]);

        InvestmentHMM model = new InvestmentHMM(numberOfStages);

        long startTime = System.nanoTime();
        Node root = model.search.solve(true);
        System.out.println("value: " + root.getValue());
        long endTime = System.nanoTime();
        double runtime = (endTime - startTime) / 1e9;
        System.out.println("runtime: " + runtime);

        System.out.println("tree size:" + model.search.getTreeSize());
        System.out.println("graph size:" + root.getSubgraphSize());
    }

    public static AndOrSearch makeAO(Solver cp, IntVar objective, Factor[] factors,
                                     Supplier<VariableBranches> branching) {
        return new AndOrSearch(cp.getStateManager(), objective, factors, branching);
    }

    public static VariableBranches getBranches(IntVar x) {
        return new VariableBranches(x, IntStream.range(x.min(), x.max() + 1).filter(y -> x.contains(y)).toArray());
    }

}
