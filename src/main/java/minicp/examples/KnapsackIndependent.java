package minicp.examples;

import java.util.Arrays;
import java.util.HashSet;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import minicp.cp.Factory;
import minicp.engine.core.Factor;
import minicp.engine.core.IntVar;
import minicp.engine.core.Solver;
import minicp.search.AndOrSearch;
import minicp.search.diagram.Node;
import minicp.util.VariableBranches;

public class KnapsackIndependent {
    public AndOrSearch search;

    public KnapsackIndependent(int numberOfStages) {
        Solver cp = Factory.makeSolver(false);

        HashSet<Integer> weight_values = new HashSet<>(Arrays.asList(15, 20, 25, 30, 35));
        HashSet<Integer> temp_weight_values = new HashSet<>(Arrays.asList(0, 15, 20, 25, 30, 35));
        HashSet<Integer> reward_values = new HashSet<>(Arrays.asList(10, 15, 20));

        IntVar[] w = new IntVar[numberOfStages];
        IntVar[] r = new IntVar[numberOfStages];
        IntVar[] d = new IntVar[numberOfStages];
        for (int t = 0; t < numberOfStages; t++) {
            // weight of an item
            w[t] = Factory.makeIntVar(cp, weight_values, false, "weight");
            // value of an item
            r[t] = Factory.makeIntVar(cp, reward_values, false, "value");
            // decision of taking an item
            d[t] = Factory.makeIntVar(cp, 0, 1, true, "decision");
        }


        double[] wwPotentials = new double[] {0.14003,0.17193,0.20243,0.23075,0.25486};
        Factor[] wwFactor = new Factor[numberOfStages];
        for (int t = 0; t < numberOfStages; t++) {
            wwFactor[t] = new Factor(w[t], new IntVar[] { }, wwPotentials);
            cp.post(wwFactor[t]);
        }

        double[] rrPotentials = new double[] { 0.25873,0.33617,0.4051 };
        Factor[] rrFactor = new Factor[numberOfStages];
        for (int t = 0; t < numberOfStages; t++) {
            rrFactor[t] = new Factor(r[t], new IntVar[] {}, rrPotentials);
            cp.post(rrFactor[t]);
        }

        Factor[] factors = new Factor[2 * numberOfStages];
        int index = 0;
        for (int t = 0; t < numberOfStages; t++) {
            factors[index] = rrFactor[t];
            index++;
            factors[index] = wwFactor[t];
            index++;
        }

        IntVar[] vars = new IntVar[numberOfStages * 3];
        index = 0;
        for (int t = 0; t < numberOfStages; t++) {
            vars[index] = d[t];
            index++;
            vars[index] = r[t];
            index++;
            vars[index] = w[t];
            index++;
        }

        // add constraints
        int capacity = numberOfStages * 25;
        IntVar[] accumaltedWeights = new IntVar[numberOfStages];

        accumaltedWeights[0] = Factory.makeIntVar(cp, 0, capacity, true, "accum_weight");
        cp.post(Factory.mul(new IntVar[] {w[0]}, new IntVar[] {d[0]}, accumaltedWeights[0]));

        for (int t = 0; t < numberOfStages-1; t++) {
            IntVar w_temp = Factory.makeIntVar(cp, temp_weight_values, true, "weight_temp");
            cp.post(Factory.mul(new IntVar[] {w[t+1]}, new IntVar[] {d[t+1]}, w_temp));
            accumaltedWeights[t+1] = Factory.makeIntVar(cp, 0, capacity, true, "accum_weight");
            cp.post(Factory.sum(new IntVar[] {accumaltedWeights[t],w_temp} , accumaltedWeights[t+1]));
        }


        // add utility
        int maximum_value = numberOfStages * 20;
        //IntVar utility = Factory.makeIntVar(cp, 0, maximum_value, true, "utility");
        //cp.post(Factory.mul(d, r, utility));

        IntVar[] accumaltedUtility = new IntVar[numberOfStages];

        accumaltedUtility[0] = Factory.makeIntVar(cp, 0, maximum_value, true, "accum_utility");
        cp.post(Factory.mul(new IntVar[] {r[0]}, new IntVar[] {d[0]}, accumaltedUtility[0]));

        for (int t = 0; t < numberOfStages-1; t++) {
            IntVar u_temp = Factory.makeIntVar(cp, 0, maximum_value, true, "utility_temp");
            cp.post(Factory.mul(new IntVar[] {r[t+1]}, new IntVar[] {d[t+1]}, u_temp));
            accumaltedUtility[t+1] = Factory.makeIntVar(cp, 0, capacity, true, "accum_utility");
            cp.post(Factory.sum(new IntVar[] {accumaltedUtility[t],u_temp} , accumaltedUtility[t+1]));
        }

        this.search = makeAO(cp, accumaltedUtility[numberOfStages-1], factors, () -> {
                for (int i = 0; i < vars.length; i++)
                    if (!vars[i].isFixed())
                        return getBranches(vars[i]);
                return VariableBranches.NOBRANCH;
            });
    }

    public static void main(String[] args) {

        if (args.length != 1) {
            System.err.println("argument [number of stages] is missing");
            System.exit(1);
        }
        int numberOfStages = Integer.parseInt(args[0]);

        KnapsackIndependent model = new KnapsackIndependent(numberOfStages);

        long startTime = System.nanoTime();
        Node root = model.search.solve(true);
        System.out.println("value: " + root.getValue());
        long endTime = System.nanoTime();
        double runtime = (endTime - startTime) / 1e9;
        System.out.println("runtime: " + runtime);

        System.out.println("tree size:" + model.search.getTreeSize());
        System.out.println("graph size:" + root.getSubgraphSize());

    }

    public static AndOrSearch makeAO(Solver cp, IntVar objective, Factor[] factors,
                                     Supplier<VariableBranches> branching) {
        return new AndOrSearch(cp.getStateManager(), objective, factors, branching);
    }

    public static VariableBranches getBranches(IntVar x) {
        return new VariableBranches(x, IntStream.range(x.min(), x.max() + 1).filter(y -> x.contains(y)).toArray());
    }

}
