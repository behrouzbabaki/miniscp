/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2018. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */

package minicp.examples;

import java.util.function.Supplier;
import java.util.stream.IntStream;

import minicp.cp.Factory;
import minicp.engine.core.Factor;
import minicp.engine.core.IntVar;
import minicp.engine.core.Solver;
import minicp.search.AndOrSearch;
import minicp.search.diagram.Node;
import minicp.util.VariableBranches;

public class CacheToy {
    public static void main(String[] args) {

        Solver cp = Factory.makeSolver(false);

        IntVar[] x = Factory.makeIntVarArray(cp, 3, 0, 1, true, "x");
        IntVar[] s = Factory.makeIntVarArray(cp, 3, 0, 1, false, "s");

        Factor[] factors = new Factor[3];

        double[] sPotentials = new double[] { 0.55, 0.45 };
        factors[0] = new Factor(s[0], new IntVar[] {}, sPotentials);
        cp.post(factors[0]);

        double[] ssPotentials = new double[] { 0.25, 0.75, 0.65, 0.35 };
        for (int i = 1; i < 3; i++) {
            factors[i] = new Factor(s[i], new IntVar[] { s[i - 1] }, ssPotentials);
            cp.post(factors[i]);
        }

        cp.post(Factory.lessOrEqual(x[0], x[1]));
        cp.post(Factory.lessOrEqual(x[1], x[2]));

        IntVar[] vars = new IntVar[6];
        for (int i = 0; i < 3; i++) {
            vars[2 * i] = s[i];
            vars[2 * i + 1] = x[i];
        }

        AndOrSearch search = makeAO(cp, x[2], factors, () -> {
                for (int i = 0; i < vars.length; i++) {
                    if (!vars[i].isFixed())
                        return getBranches(vars[i]);
                }
                return VariableBranches.NOBRANCH;
            });

        Node root = search.solve();
        root.toDot("output/toy.gv");

    }

    public static AndOrSearch makeAO(Solver cp, IntVar objective, Factor[] factors,
                                     Supplier<VariableBranches> branching) {
        return new AndOrSearch(cp.getStateManager(), objective, factors, branching);
    }

    public static VariableBranches getBranches(IntVar x) {
        return new VariableBranches(x, IntStream.range(x.min(), x.max() + 1).filter(y -> x.contains(y)).toArray());
    }

}
