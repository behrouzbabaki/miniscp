package minicp.examples;

public class MazeHelper {

public static class Location{
		
		int position_x;
		int position_y;
		
		public Location(int position_x, int position_y) {
			this.position_x = position_x;
			this.position_y = position_y;
		}
		
	}

	public Location[] Maze_1() {
		//from Changhe Yuan paper
		Location[] free_spaces = new Location[23];
		int dimention_x = 9;
		int dimention_y = 7;
		Location goal_state = new Location(4,7);
		
		//Locations which are free spaces on the maze
				free_spaces[0] = new Location(1,2);
				free_spaces[1] = new Location(1,3);
				free_spaces[2] = new Location(1,4);
				free_spaces[3] = new Location(1,5);
				free_spaces[4] = new Location(1,6);
				free_spaces[5] = new Location(2,1);
				free_spaces[6] = new Location(2,2);
				free_spaces[7] = new Location(2,4);
				free_spaces[8] = new Location(2,6);
				free_spaces[9] = new Location(2,7);
				free_spaces[10] = new Location(3,2);
				free_spaces[11] = new Location(3,4);
				free_spaces[12] = new Location(3,6);
				free_spaces[13] = new Location(4,1);
				free_spaces[14] = new Location(4,2);
				free_spaces[15] = new Location(4,4);
				free_spaces[16] = new Location(4,6);
				free_spaces[17] = new Location(4,7);
				free_spaces[18] = new Location(5,2);
				free_spaces[19] = new Location(5,3);
				free_spaces[20] = new Location(5,4);
				free_spaces[21] = new Location(5,5);
				free_spaces[22] = new Location(5,6);
		
		return free_spaces;
	}
	
	
	public Location[] Maze_2() {
		//from Changhe Yuan paper
		Location[] free_spaces = new Location[25];
		int dimention_x = 9;
		int dimention_y = 7;
		Location goal_state = new Location(5,3);
		
				free_spaces[0] = new Location(1,1);
				free_spaces[1] = new Location(1,3);
				free_spaces[2] = new Location(1,4);
				free_spaces[3] = new Location(1,5);
				free_spaces[4] = new Location(1,7);
				free_spaces[5] = new Location(2,1);
				free_spaces[6] = new Location(2,3);
				free_spaces[7] = new Location(2,4);
				free_spaces[8] = new Location(2,5);
				free_spaces[9] = new Location(2,7);
				free_spaces[10] = new Location(3,1);
				free_spaces[11] = new Location(3,2);
				free_spaces[12] = new Location(3,3);
				free_spaces[13] = new Location(3,5);
				free_spaces[14] = new Location(3,6);
				free_spaces[15] = new Location(3,7);
				free_spaces[16] = new Location(4,1);
				free_spaces[17] = new Location(4,3);
				free_spaces[18] = new Location(4,4);
				free_spaces[19] = new Location(4,7);
				free_spaces[20] = new Location(5,1);
				free_spaces[21] = new Location(5,3);
				free_spaces[22] = new Location(5,4);
				free_spaces[23] = new Location(5,5);
				free_spaces[24] = new Location(5,7);
		
		return free_spaces;
	}
	
	
	public Location[] Maze_3() {
		//from David Poole paper
		Location[] free_spaces = new Location[23];
		int dimention_x = 7;
		int dimention_y = 5;
		Location goal_state = new Location(3,6);
		
		free_spaces[0] = new Location(0,1);
		free_spaces[1] = new Location(0,2);
		free_spaces[2] = new Location(0,3);
		free_spaces[3] = new Location(0,4);
		free_spaces[4] = new Location(0,5);
		free_spaces[5] = new Location(1,0);
		free_spaces[6] = new Location(1,1);
		free_spaces[7] = new Location(1,3);
		free_spaces[8] = new Location(1,5);
		free_spaces[9] = new Location(1,6);
		free_spaces[10] = new Location(2,1);
		free_spaces[11] = new Location(2,3);
		free_spaces[12] = new Location(2,5);
		free_spaces[13] = new Location(3,0);
		free_spaces[14] = new Location(3,1);
		free_spaces[15] = new Location(3,3);
		free_spaces[16] = new Location(3,5);
		free_spaces[17] = new Location(3,6);
		free_spaces[18] = new Location(4,1);
		free_spaces[19] = new Location(4,2);
		free_spaces[20] = new Location(4,3);
		free_spaces[21] = new Location(4,4);
		free_spaces[22] = new Location(4,5);
		
		return free_spaces;
		
	}
	
	public Location[] Maze_4() {
		//from David Poole paper
		Location[] free_spaces = new Location[26];
		int dimention_x = 7;
		int dimention_y = 5;
		Location goal_state = new Location(4,2);
		
		free_spaces[0] = new Location(0,0);
		free_spaces[1] = new Location(0,2);
		free_spaces[2] = new Location(0,3);
		free_spaces[3] = new Location(0,4);
		free_spaces[4] = new Location(0,6);
		free_spaces[5] = new Location(1,0);
		free_spaces[6] = new Location(1,2);
		free_spaces[7] = new Location(1,3);
		free_spaces[8] = new Location(1,4);
		free_spaces[9] = new Location(1,6);
		free_spaces[10] = new Location(2,0);
		free_spaces[11] = new Location(2,1);
		free_spaces[12] = new Location(2,2);
		free_spaces[13] = new Location(2,4);
		free_spaces[14] = new Location(2,5);
		free_spaces[15] = new Location(2,6);
		free_spaces[16] = new Location(3,0);
		free_spaces[17] = new Location(3,2);
		free_spaces[18] = new Location(3,3);
		free_spaces[19] = new Location(3,5);
		free_spaces[20] = new Location(3,6);
		free_spaces[21] = new Location(4,0);
		free_spaces[22] = new Location(4,2);
		free_spaces[23] = new Location(4,3);
		free_spaces[24] = new Location(4,4);
		free_spaces[25] = new Location(4,6);
		
		return free_spaces;
		
	}
	
	
	public Location[] Maze_5() {
		//from David Poole paper
		Location[] free_spaces = new Location[27];
		int dimention_x = 7;
		int dimention_y = 5;
		Location goal_state = new Location(1,3);
		
		free_spaces[0] = new Location(0,0);
		free_spaces[1] = new Location(0,1);
		free_spaces[2] = new Location(0,2);
		free_spaces[3] = new Location(0,3);
		free_spaces[4] = new Location(0,5);
		free_spaces[5] = new Location(0,6);
		free_spaces[6] = new Location(1,0);
		free_spaces[7] = new Location(1,2);
		free_spaces[8] = new Location(1,3);
		free_spaces[9] = new Location(1,5);
		free_spaces[10] = new Location(1,6);
		free_spaces[11] = new Location(2,0);
		free_spaces[12] = new Location(2,1);
		free_spaces[13] = new Location(2,2);
		free_spaces[14] = new Location(2,4);
		free_spaces[15] = new Location(2,5);
		free_spaces[16] = new Location(2,6);
		free_spaces[17] = new Location(3,0);
		free_spaces[18] = new Location(3,2);
		free_spaces[19] = new Location(3,3);
		free_spaces[20] = new Location(3,4);
		free_spaces[21] = new Location(3,6);
		free_spaces[22] = new Location(4,1);
		free_spaces[23] = new Location(4,2);
		free_spaces[24] = new Location(4,3);
		free_spaces[25] = new Location(4,4);
		free_spaces[26] = new Location(4,5);
		
		return free_spaces;
		
	}
	
	public Location[] Maze_6() {
		//from David Poole paper
		Location[] free_spaces = new Location[23];
		int dimention_x = 7;
		int dimention_y = 5;
		Location goal_state = new Location(1,3);
		
		free_spaces[0] = new Location(0,1);
		free_spaces[1] = new Location(0,2);
		free_spaces[2] = new Location(0,3);
		free_spaces[3] = new Location(0,5);
		free_spaces[4] = new Location(1,0);
		free_spaces[5] = new Location(1,1);
		free_spaces[6] = new Location(1,3);
		free_spaces[7] = new Location(1,4);
		free_spaces[8] = new Location(1,5);
		free_spaces[9] = new Location(1,6);
		free_spaces[10] = new Location(2,1);
		free_spaces[11] = new Location(2,2);
		free_spaces[12] = new Location(2,3);
		free_spaces[13] = new Location(2,5);
		free_spaces[14] = new Location(3,0);
		free_spaces[15] = new Location(3,1);
		free_spaces[16] = new Location(3,5);
		free_spaces[17] = new Location(3,6);
		free_spaces[18] = new Location(4,1);
		free_spaces[19] = new Location(4,2);
		free_spaces[20] = new Location(4,3);
		free_spaces[21] = new Location(4,4);
		free_spaces[22] = new Location(4,5);
		
		return free_spaces;
		
	}
	
}
