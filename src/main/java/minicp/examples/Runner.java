package minicp.examples;

import java.util.Arrays;

public class Runner {
	public static void main(String[] args) {
		if (args.length < 3) {
			System.out.println("usage: ... PROBLEM DISTRIBUTION NSTAGES");
			System.exit(1);
		}
		
		String problem = args[0];
		String distribution = args[1];
		
		args = Arrays.copyOfRange(args, 2, args.length);
		if (problem.equals("knapsack")) {
			if (distribution.equals("hmm"))
				KnapsackHMM.main(args);
			else if (distribution.equals("chain"))
				KnapsackChain.main(args);
			else if (distribution.equals("independent"))
				KnapsackIndependent.main(args);
		}
		else if (problem.equals("investment")) {
			if (distribution.equals("hmm"))
				InvestmentHMM.main(args);
			else if (distribution.equals("chain"))
				InvestmentChain.main(args);
			else if (distribution.equals("independent"))
				InvestmentIndependent.main(args);
		}
	}
}
