package minicp.examples;

import java.util.Arrays;
import java.util.HashSet;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import minicp.cp.Factory;
import minicp.engine.core.Factor;
import minicp.engine.core.IntVar;
import minicp.engine.core.Solver;
import minicp.search.AndOrSearch;
import minicp.search.diagram.Node;
import minicp.util.VariableBranches;

public class InvestmentIndependent {
    public AndOrSearch search;

    public InvestmentIndependent(int numberOfStages) {
        Solver cp = Factory.makeSolver(false);

        HashSet<Integer> option_A_values = new HashSet<>(Arrays.asList(10, 15, 20, 25));
        HashSet<Integer> option_B_values = new HashSet<>(Arrays.asList(20, 25, 30, 35));

        HashSet<Integer> temp_A_values = new HashSet<>(Arrays.asList(0, 10, 15, 20, 25));
        HashSet<Integer> temp_B_values = new HashSet<>(Arrays.asList(0, 20, 25, 30, 35));

        IntVar[] b = new IntVar[numberOfStages];
        IntVar[] a = new IntVar[numberOfStages];
        IntVar[] db = new IntVar[numberOfStages];
        IntVar[] da = new IntVar[numberOfStages];

        for (int t = 0; t < numberOfStages; t++) {
            a[t] = Factory.makeIntVar(cp, option_A_values, false, "opt_a");
            b[t] = Factory.makeIntVar(cp, option_B_values, false, "opt_b");
            da[t] = Factory.makeIntVar(cp, 0, 1, true, "decision_a");
            db[t] = Factory.makeIntVar(cp, 0, 1, true, "decision_b");
        }

        double[] aaPotentials = new double[] {0.27586, 0.17784, 0.1837, 0.36260000000000003};
        Factor[] aaFactor = new Factor[numberOfStages];
        for (int t = 0; t < numberOfStages; t++) {
            aaFactor[t] = new Factor(a[t], new IntVar[] {}, aaPotentials);
            cp.post(aaFactor[t]);
        }

        double[] bbPotentials = new double[] {0.27586, 0.17784, 0.1837, 0.36260000000000003};
        Factor[] bbFactor = new Factor[numberOfStages];
        for (int t = 0; t < numberOfStages; t++) {
            bbFactor[t] = new Factor(b[t], new IntVar[] {}, bbPotentials);
            cp.post(bbFactor[t]);
        }


        Factor[] factors = new Factor[2 * numberOfStages];
        int index = 0;
        for (int t = 0; t < numberOfStages; t++) {
            factors[index] = aaFactor[t];
            index++;
            factors[index] = bbFactor[t];
            index++;
        }

        IntVar[] vars = new IntVar[numberOfStages * 4];
        index = 0;
        for (int t = 0; t < numberOfStages; t++) {
            vars[index] = da[t];
            index++;
            vars[index] = db[t];
            index++;
            vars[index] = a[t];
            index++;
            vars[index] = b[t];
            index++;

        }

        // add constraints
        // a nor b

        for (int t = 0; t < numberOfStages; t++) {
            cp.post(Factory.notEqual(da[t], db[t]));
        }


        // sum b < sum a

        int maximum_value = numberOfStages * 35;


        IntVar[] accumalteda = new IntVar[numberOfStages];
        accumalteda[0] = Factory.makeIntVar(cp, 0, maximum_value, true, "accum_a0");
        cp.post(Factory.mul(new IntVar[] {a[0]}, new IntVar[] {da[0]}, accumalteda[0]));

        IntVar[] accumaltedb = new IntVar[numberOfStages];
        accumaltedb[0] = Factory.makeIntVar(cp, 0, maximum_value, true, "accum_b0");
        cp.post(Factory.mul(new IntVar[] {b[0]}, new IntVar[] {db[0]}, accumaltedb[0]));

        for (int t = 0; t < numberOfStages-1; t++) {
            IntVar a_temp = Factory.makeIntVar(cp, temp_A_values , true, "a_temp"+(t+1));
            cp.post(Factory.mul(new IntVar[] {a[t+1]}, new IntVar[] {da[t+1]}, a_temp));
            accumalteda[t+1] = Factory.makeIntVar(cp, 0, maximum_value, true, "accum_a"+(t+1));
            cp.post(Factory.sum(new IntVar[] {accumalteda[t],a_temp} , accumalteda[t+1]));
        }

        for (int t = 0; t < numberOfStages-1; t++) {
            IntVar b_temp = Factory.makeIntVar(cp, temp_B_values , true, "b_temp"+(t+1));
            cp.post(Factory.mul(new IntVar[] {b[t+1]}, new IntVar[] {db[t+1]}, b_temp));
            accumaltedb[t+1] = Factory.makeIntVar(cp, 0, maximum_value, true, "accum_b"+(t+1));
            cp.post(Factory.sum(new IntVar[] {accumaltedb[t],b_temp} , accumaltedb[t+1]));
        }

        cp.post(Factory.lessOrEqual(accumaltedb[numberOfStages-1],accumalteda[numberOfStages-1]));


        // add utility: sum b + sum a

        IntVar utility = Factory.makeIntVar(cp, 0, 2* maximum_value , true, "utility");
        cp.post(Factory.sum(new IntVar[] {accumaltedb[numberOfStages-1], accumalteda[numberOfStages-1]}, utility));

        this.search = makeAO(cp, utility, factors, () -> {
                for (int i = 0; i < vars.length; i++)
                    if (!vars[i].isFixed())
                        return getBranches(vars[i]);
                return VariableBranches.NOBRANCH;
            });
    }

    public static void main(String[] args) {

        if (args.length != 1) {
            System.err.println("argument [number of stages] is missing");
            System.exit(1);
        }
        int numberOfStages = Integer.parseInt(args[0]);

        InvestmentIndependent model = new InvestmentIndependent(numberOfStages);

        long startTime = System.nanoTime();
        Node root = model.search.solve(true);
        System.out.println("value: " + root.getValue());
        long endTime = System.nanoTime();
        double runtime = (endTime - startTime) / 1e9;
        System.out.println("runtime: " + runtime);

        System.out.println("tree size:" + model.search.getTreeSize());
        System.out.println("graph size:" + root.getSubgraphSize());


    }

    public static AndOrSearch makeAO(Solver cp, IntVar objective, Factor[] factors,
                                     Supplier<VariableBranches> branching) {
        return new AndOrSearch(cp.getStateManager(), objective, factors, branching);
    }

    public static VariableBranches getBranches(IntVar x) {
        return new VariableBranches(x, IntStream.range(x.min(), x.max() + 1).filter(y -> x.contains(y)).toArray());
    }

}
