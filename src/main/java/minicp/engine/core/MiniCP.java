/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2018. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */

package minicp.engine.core;

import minicp.cp.Factory;
import minicp.search.Objective;
import minicp.state.StateManager;
import minicp.state.StateStack;
import minicp.util.exception.InconsistencyException;
import minicp.util.Procedure;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Stream;


public class MiniCP implements Solver {

    private Queue<Constraint> propagationQueue = new ArrayDeque<>();
    private List<Procedure> fixPointListeners = new LinkedList<>();

    private final StateManager sm;
    private final ArrayList<IntVar> vars;
    private int nVars;
    private int maxDomainValue;

    public MiniCP(StateManager sm) {
        this.sm = sm;
        vars = new ArrayList<>();
        nVars = 0;
        maxDomainValue = Integer.MIN_VALUE;
    }

    @Override
    public StateManager getStateManager() {
        return sm;
    }

    public void schedule(Constraint c) {
        if (c.isActive() && !c.isScheduled()) {
            c.setScheduled(true);
            propagationQueue.add(c);
        }
    }

    @Override
    public void onFixPoint(Procedure listener) {
        fixPointListeners.add(listener);
    }

    private void notifyFixPoint() {
        fixPointListeners.forEach(s -> s.call());
    }

    @Override
    public void fixPoint() {
        try {
            notifyFixPoint();
            while (!propagationQueue.isEmpty()) {
                propagate(propagationQueue.remove());
            }
        } catch (InconsistencyException e) {
            // empty the queue and unset the scheduled status
            while (!propagationQueue.isEmpty())
                propagationQueue.remove().setScheduled(false);
            throw e;
        }
    }

    private void propagate(Constraint c) {
        c.setScheduled(false);
        if (c.isActive())
            c.propagate();
    }

    @Override
    public Objective minimize(IntVar x) {
        return new Minimize(x);
    }

    @Override
    public Objective maximize(IntVar x) {
        return minimize(Factory.minus(x));
    }

    @Override
    public void post(Constraint c) {
        post(c, true);
    }

    @Override
    public void post(Constraint c, boolean enforceFixPoint) {
        c.post();
        if (enforceFixPoint) fixPoint();
    }

    @Override
    public void post(BoolVar b) {
        b.fix(true);
        fixPoint();
    }

	@Override
	public void addVariable(IntVar v) {
		boolean found = false;
		for (int i = 0, s = vars.size(); !found && i < s; i++)
			found = (vars.get(i) == v);

		if (!found) {
			vars.add(v);
			nVars++;
			maxDomainValue = Integer.max(maxDomainValue, v.max() + 1);
		}
	}
	
	@Override
	public Context getContext() {
		int[] contextArray = new int[nVars];
		for (int i=0; i<nVars; i++) {
			IntVar v = vars.get(i);
			if (v.isFixed() && v.isActive())
				contextArray[i] = v.min();
			else
				contextArray[i] = maxDomainValue;
		}
		return new Context(contextArray);
	}
}
