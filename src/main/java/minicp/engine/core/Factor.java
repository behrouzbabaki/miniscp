package minicp.engine.core;

import minicp.state.State;
import minicp.state.StateInt;
import minicp.state.StateManager;

import java.util.ArrayList;

public class Factor extends AbstractConstraint {

    private IntVar[] x;
    private int nVar;
    private ArrayList<State<Boolean> > isFixed;
    private StateInt nBound;
    // TODO use a map for variableValues
    private int[][] variableValues;
    private int[] numValues;
    private int[] boundVarIndices;
    private double[] probabilities;
    private double selectedProbability;

    public Factor(IntVar childVar, IntVar[] parentVars, double[] probabilities) {

        super(childVar.getSolver());
        this.probabilities = probabilities;

        StateManager sm = this.getSolver().getStateManager();
        this.nVar = parentVars.length + 1;
        boundVarIndices = new int[nVar];

        this.x = new IntVar[nVar];
        for (int i = 0; i < nVar - 1; i++)
            x[i] = parentVars[i];
        x[nVar - 1] = childVar;

        variableValues = new int[nVar][];
        numValues = new int[nVar];
        for (int i = 0; i < nVar; i++) {
            int s = x[i].size();
            numValues[i] = s;
            variableValues[i] = new int[s];
            x[i].fillArray(variableValues[i]);
        }

        verifyVarsAndProbabilities();

        int boundCount = 0;
        isFixed = new ArrayList<State<Boolean> >(nVar);
        for (int i = 0; i < nVar; i++) {
            isFixed.add(sm.makeStateRef(x[i].isFixed()));
            if (x[i].isFixed()) {
                setBoundVarIndex(i);
                boundCount++;
            }
        }
        nBound = sm.makeStateInt(boundCount);
    }

    private void verifyVarsAndProbabilities() {
        for (IntVar var : x)
            assert (var.isRandom());

        int nRows = 1;
        for (int i = 0; i < nVar; i++)
            nRows *= numValues[i];
        assert (probabilities.length == nRows);

        int nChildValues = numValues[nVar - 1];
        double sumOfProbs;
        int i = 0;
        while (i < nRows) {
            sumOfProbs = 0;
            for (int j = 0; j < nChildValues; j++) {
                sumOfProbs += probabilities[i];
                i++;
            }
            assert (Math.abs(1 - sumOfProbs) < 1e-5);
        }
    }

    private void setBoundVarIndex(int i) {
        int value = x[i].max();
        boolean done = false;
        for (int j = 0; !done && j < numValues[i]; j++)
            if (value == variableValues[i][j]) {
                boundVarIndices[i] = j;
                done = true;
            }
    }

    @Override
    public void post() {
        for (IntVar var : x)
            var.propagateOnFix(this);
        propagate();
    }

    // TODO Use the method of StateInt index (like in the Sum constraint)
    @Override
    public void propagate() {

        for (int i = 0; i < nVar; i++)
            if (x[i].isFixed() && !isFixed.get(i).value()) {
                isFixed.get(i).setValue(true);
                setBoundVarIndex(i);
                nBound.increment();
            }

        if (nBound.value() == nVar) {
            int selectedIndex = 0;
            int coefficient = 1;
            for (int i = nVar - 1; i >= 0; i--) {
                selectedIndex += boundVarIndices[i] * coefficient;
                coefficient *= numValues[i];
            }
            this.selectedProbability = probabilities[selectedIndex];

            this.setActive(false);
        }
    }

    public double getProbability() {
        return selectedProbability;
    }

}
