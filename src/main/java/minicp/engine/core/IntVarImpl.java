/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2018. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */

package minicp.engine.core;

import minicp.state.State;
import minicp.state.StateInt;
import minicp.state.StateStack;
import minicp.util.Procedure;
import minicp.util.exception.InconsistencyException;
import minicp.util.exception.NotImplementedException;

import java.security.InvalidParameterException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

/**
 * Implementation of a variable
 * with a {@link SparseSetDomain}.
 */
public class IntVarImpl implements IntVar {

    private final Solver cp;
    private final IntDomain domain;
    private final boolean isDecision;
    private final String name;
    private final StateStack<Constraint> onDomain;
    private final StateStack<Constraint> onFix;
    private final StateStack<Constraint> onBound;

    private Map<Constraint, State<Boolean> > constraintMap;
    private StateInt numActiveConstraints;

    private final DomainListener domListener = new DomainListener() {
            @Override
            public void empty() {
                throw InconsistencyException.INCONSISTENCY; // Integer Vars cannot be empty
            }

            @Override
            public void fix() {
                scheduleAll(onFix);
                notifyConstraints();
            }

            @Override
            public void change() {
                change(false);
            }

            @Override
            public void change(boolean noFail) {
                if (isDecision || noFail)
                    scheduleAll(onDomain);
                else
                    throw InconsistencyException.INCONSISTENCY;
            }

            @Override
            public void changeMin() {
                scheduleAll(onBound);
            }

            @Override
            public void changeMax() {
                scheduleAll(onBound);
            }
    };

    /**
     * Creates a variable with the elements {@code {0,...,n-1}}
     * as initial domain.
     *
     * @param cp the solver in which the variable is created
     * @param n  the number of values with {@code n > 0}
     */
    public IntVarImpl(Solver cp, int n) {
        this(cp, 0, n - 1);
    }

    public IntVarImpl(Solver cp, int n, boolean isDecision, String name) {
        this(cp, 0, n - 1, isDecision, name);
    }

    /**
     * Creates a variable with the elements {@code {min,...,max}}
     * as initial domain.
     *
     * @param cp the solver in which the variable is created
     * @param min the minimum value of the domain
     * @param max the maximum value of the domain with {@code max >= min}
     */
    public IntVarImpl(Solver cp, int min, int max, boolean isDecision, String name) {
        if (min == Integer.MIN_VALUE || max == Integer.MAX_VALUE) throw new InvalidParameterException("consider reducing the domains, Integer.MIN _VALUE and Integer.MAX_VALUE not allowed");
        if (min > max) throw new InvalidParameterException("at least one setValue in the domain");
        this.cp = cp;
        this.isDecision = isDecision;
        this.name = name;
        domain = new SparseSetDomain(cp.getStateManager(), min, max);
        onDomain = new StateStack<>(cp.getStateManager());
        onFix = new StateStack<>(cp.getStateManager());
        onBound = new StateStack<>(cp.getStateManager());
        cp.addVariable(this);
        constraintMap = new HashMap<Constraint, State<Boolean> >();
        numActiveConstraints = cp.getStateManager().makeStateInt(0);
    }

    public IntVarImpl(Solver cp, int min, int max) {
        this(cp, min, max, true, "");
    }

    /**
     * Creates a variable with a given set of values as initial domain.
     *
     * @param cp the solver in which the variable is created
     * @param values the initial values in the domain, it must be nonempty
     */
    public IntVarImpl(Solver cp, Set<Integer> values) {
        this(cp, values, true, "");
    }

    public IntVarImpl(Solver cp, Set<Integer> values, boolean isDecision, String name) {

        int min = Collections.min(values);
        int max = Collections.max(values);

        if (min > max)
            throw new InvalidParameterException("at least one setValue in the domain");

        this.cp = cp;
        this.name = name;
        this.isDecision = isDecision;

        domain = new SparseSetDomain(cp.getStateManager(), min, max);
        onDomain = new StateStack<>(cp.getStateManager());
        onFix = new StateStack<>(cp.getStateManager());
        onBound = new StateStack<>(cp.getStateManager());

        cp.addVariable(this);
        constraintMap = new HashMap<Constraint, State<Boolean> >();
        numActiveConstraints = cp.getStateManager().makeStateInt(0);

        for (int i = min; i <= max; i++)
            if (!values.contains(i))
                remove(i, true);
    }

    @Override
    public Solver getSolver() {
        return cp;
    }

    @Override
    public boolean isDecision() {
        return isDecision;
    }

    @Override
    public boolean isRandom() {
        return !isDecision;
    }

    @Override
    public boolean isActive() {
        return (numActiveConstraints.value() > 0);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public boolean isFixed() {
        return domain.isSingleton();
    }

    @Override
    public String toString() {
        return domain.toString();
    }

    @Override
    public void whenFixed(Procedure f) {
        onFix.push(constraintClosure(f));
    }

    @Override
    public void whenBoundChange(Procedure f) {
        onBound.push(constraintClosure(f));
    }

    @Override
    public void whenDomainChange(Procedure f) {
        onDomain.push(constraintClosure(f));
    }

    private Constraint constraintClosure(Procedure f) {
        Constraint c = new ConstraintClosure(cp, f);
        getSolver().post(c, false);
        return c;
    }

    public void addToConstraints(Constraint c) {
        if (constraintMap.containsKey(c))
            return;

        constraintMap.put(c, cp.getStateManager().makeStateRef(true));
        numActiveConstraints.increment();
        c.addToScope(this);
    }


    @Override
    public void propagateOnDomainChange(Constraint c) {
        onDomain.push(c);
        addToConstraints(c);
    }

    @Override
    public void propagateOnFix(Constraint c) {
        onFix.push(c);
        addToConstraints(c);
    }

    @Override
    public void propagateOnBoundChange(Constraint c) {
        onBound.push(c);
        addToConstraints(c);
    }


    protected void scheduleAll(StateStack<Constraint> constraints) {
        for (int i = 0; i < constraints.size(); i++)
            cp.schedule(constraints.get(i));
    }

    private void notifyConstraints() {
        for (Constraint c : constraintMap.keySet())
            c.setFixed(this);
    }

    @Override
    public int min() {
        return domain.min();
    }

    @Override
    public int max() {
        return domain.max();
    }

    @Override
    public int size() {
        return domain.size();
    }

    @Override
    public int fillArray(int[] dest) {
        int[] values = IntStream.range(min(), max() + 1).filter(i -> contains(i)).toArray();
        System.arraycopy(values, 0, dest, 0, values.length);
        return values.length;
    }

    @Override
    public boolean contains(int v) {
        return domain.contains(v);
    }

    @Override
    public void remove(int v) {
        domain.remove(v, domListener);
    }

    public void remove(int v, boolean noFail) {
        domain.remove(v, domListener, noFail);
    }

    @Override
    public void fix(int v) {
        domain.removeAllBut(v, domListener, false);
    }

    @Override
    public void fix(int v, boolean noFail) {
        domain.removeAllBut(v, domListener, noFail);
    }

    @Override
    public void removeBelow(int v) {
        domain.removeBelow(v, domListener);
    }

    @Override
    public void removeAbove(int v) {
        domain.removeAbove(v, domListener);
    }

    @Override
    public void setInactive(Constraint c) {
        State<Boolean> isConstraintActive = constraintMap.get(c);
        if (!isConstraintActive.value())
            return;

        isConstraintActive.setValue(false);
        numActiveConstraints.decrement();
    }
}
