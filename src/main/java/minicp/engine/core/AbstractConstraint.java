/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2018. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */

package minicp.engine.core;

import minicp.state.State;
import minicp.state.StateInt;

import java.util.HashMap;
import java.util.Map;

/**
 * Abstract class the most of the constraints
 * should extend.
 */
public abstract class AbstractConstraint implements Constraint {

    /**
     * The solver in which the constraint is created
     */
    private final Solver cp;
    private boolean scheduled = false;
    private final State<Boolean> active;
    private Map<IntVar, State<Boolean> > variableMap;
    private int numVars;
    private final StateInt numFixedVars;


    public AbstractConstraint(Solver cp) {
        this.cp = cp;
        active = cp.getStateManager().makeStateRef(true);
        numVars = 0;
        numFixedVars = cp.getStateManager().makeStateInt(0);
        variableMap = new HashMap<IntVar, State<Boolean> >();
    }

    public void post() {
    }

    public Solver getSolver() {
        return cp;
    }

    public void propagate() {
    }

    public void setScheduled(boolean scheduled) {
        this.scheduled = scheduled;
    }

    public boolean isScheduled() {
        return scheduled;
    }

    public void setActive(boolean active) {
        this.active.setValue(active);
    }

    public boolean isActive() {
        return active.value();
    }

	public void checkActivity() {
		if (numFixedVars.value() == numVars)
			for (IntVar var : variableMap.keySet())
				var.setInactive(this);
	}

	public void addToScope(IntVar v) {
		if (variableMap.containsKey(v))
			return;

		numVars++;
		if (v.isFixed())
			numFixedVars.increment();

		State<Boolean> isFixed = cp.getStateManager().makeStateRef(v.isFixed());
		variableMap.put(v, isFixed);

		checkActivity();
	}

	public void setFixed(IntVar v) {
		State<Boolean> isFixed = variableMap.get(v);
		if (isFixed.value())
			return;

		isFixed.setValue(true);
		numFixedVars.increment();
		checkActivity();
	}
}
