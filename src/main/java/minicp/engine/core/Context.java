package minicp.engine.core;

import java.util.Arrays;

public class Context {

	public int[] contextArray;
	private int hashCode;

	public Context(int[] contextArray) {
		this.contextArray = contextArray;
		this.hashCode = Arrays.hashCode(contextArray);
	}


	public boolean isEmpty() {
		return (contextArray.length == 0);
	}


	@Override
	public int hashCode() {
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		Context other = (Context) obj;
		return (Arrays.equals(this.contextArray, other.contextArray));
	}
}
