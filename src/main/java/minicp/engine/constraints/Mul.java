/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2018. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 * 
 * Modifications introduced by Behrouz Babaki
 */

package minicp.engine.constraints;

import minicp.engine.core.AbstractConstraint;
import minicp.engine.core.IntVar;
import minicp.util.exception.InconsistencyException;

/**
 * Multiplication Constraint The code is mostly taken from the Choco solver
 * source (https://git.io/fhPk6)
 */
public class Mul extends AbstractConstraint {
	protected static final int MAX = Integer.MAX_VALUE - 1, MIN = Integer.MIN_VALUE + 1;
	private IntVar v0, v1, v2;

	/**
	 * Creates a multiplication constraint.
	 * <p>
	 * This constraint holds iff {@code v0 * v1 == v2}.
	 *
	 * @param v0, v1, v2
	 */
	public Mul(IntVar v0, IntVar v1, IntVar v2) {
		super(v0.getSolver());
		this.v0 = v0;
		this.v1 = v1;
		this.v2 = v2;
	}

	@Override
	public void post() {
		v0.propagateOnBoundChange(this);
		v1.propagateOnBoundChange(this);
		v2.propagateOnBoundChange(this);
		propagate();
	}

	@Override
	public void propagate() {
		boolean hasChanged = true;
		while (hasChanged) {
			hasChanged = div(v0, v2.min(), v2.max(), v1.min(), v1.max());
			hasChanged |= div(v1, v2.min(), v2.max(), v0.min(), v0.max());
			hasChanged |= mul(v2, v0.min(), v0.max(), v1.min(), v1.max());
		}
		
		if (isInstantiatedTo(v2, 0) && (isInstantiatedTo(v0, 0) || isInstantiatedTo(v1, 1))) {
			setActive(false);
		}
	}
	
	private boolean isInstantiatedTo(IntVar x, int value) {
		return (x.isFixed() && x.min() == value);
	}
	
    private boolean div(IntVar var, int a, int b, int c, int d) {
        int min, max;

        if (a <= 0 && b >= 0 && c <= 0 && d >= 0) { // case 1
            min = MIN;
            max = MAX;
            return updateLowerBound(var, min) | updateUpperBound(var, max);
        } else if (c == 0 && d == 0 && (a > 0 || b < 0)) {// case 2
        	throw new InconsistencyException();
        }
        else if (c < 0 && d > 0 && (a > 0 || b < 0)) { // case 3
            max = Math.max(Math.abs(a), Math.abs(b));
            min = -max;
            return updateLowerBound(var, min) | updateUpperBound(var, max);
        } else if (c == 0 && d != 0 && (a > 0 || b < 0)) // case 4 a
            return div(var, a, b, 1, d);
        else if (c != 0 && d == 0 && (a > 0 || b < 0)) // case 4 b
            return div(var, a, b, c, -1);
        else { // if (c > 0 || d < 0) { // case 5
            float ac = (float) a / c, ad = (float) a / d,
                    bc = (float) b / c, bd = (float) b / d;
            float low = Math.min(Math.min(ac, ad), Math.min(bc, bd));
            float high = Math.max(Math.max(ac, ad), Math.max(bc, bd));
            min = (int) Math.round(Math.ceil(low));
            max = (int) Math.round(Math.floor(high));
            if (min > max) {
            	throw new InconsistencyException();
            }
            return updateLowerBound(var, min) | updateUpperBound(var, max);
        }
    }
    
    private boolean mul(IntVar var, int a, int b, int c, int d) {
        int min = Math.min(Math.min(safeMultiply(a, c), safeMultiply(a, d)), Math.min(safeMultiply(b, c), safeMultiply(b, d)));
        int max = Math.max(Math.max(safeMultiply(a, c), safeMultiply(a, d)), Math.max(safeMultiply(b, c), safeMultiply(b, d)));
        return updateLowerBound(var, min) | updateUpperBound(var, max);
    }

	private boolean updateLowerBound(IntVar x, int value) {
		if (value <= x.min())
			return false;
		
		x.removeBelow(value);
		return true;
	}

	private boolean updateUpperBound(IntVar x, int value) {
		if (value >= x.max())
			return false;

		x.removeAbove(value);
		return false;
	}

	/**
	 * from Choco solver (https://git.io/fhPk0)
	 */
	public static int safeMultiply(int x, int y) {
		long r = (long) x * (long) y;
		// HD 2-12 Overflow iff both arguments have the opposite sign of the result
		if ((int) r != r) {
			return r > 0 ? Integer.MAX_VALUE : Integer.MIN_VALUE;
		}
		return (int) r;
	}
}
