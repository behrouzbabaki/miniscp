package minicp.util;

import minicp.engine.core.IntVar;

public final class VariableBranches {
	
	public static final VariableBranches NOBRANCH = new VariableBranches(null, new int[0]);

	private IntVar variable;
	private int[] values;
	
	public VariableBranches(IntVar v, int[] values) {
		this.variable = v;
		this.values = values;
	}
	
	public IntVar getVariable() {
		return variable;
	}
	
	public int[] getValues() {
		return values;
	}
	
	public boolean isEmpty() {
		return (values.length == 0);
	}
}
