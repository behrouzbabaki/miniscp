/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2018. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */

package minicp.search;

import java.util.ArrayList;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.function.Supplier;

import minicp.engine.core.Context;
import minicp.engine.core.Factor;
import minicp.engine.core.IntVar;
import minicp.engine.core.Solver;
import minicp.search.diagram.Edge;
import minicp.search.diagram.Node;
import minicp.state.State;
import minicp.state.StateManager;
import minicp.util.Procedure;
import minicp.util.VariableBranches;
import minicp.util.exception.InconsistencyException;
import minicp.util.exception.NotImplementedException;

/**
 * Depth First Search Branch and Bound implementation
 */
public class AndOrSearch {

    private IntVar objectiveVar;
    private Supplier<VariableBranches> branching;
    private StateManager sm;
    private Solver cp;
    private boolean performCaching;
    private boolean performBounding;

    private List<Procedure> solutionListeners = new LinkedList<Procedure>();
    private List<Procedure> failureListeners = new LinkedList<Procedure>();
    private Factor[] factors;
    private ArrayList<State<Boolean> > isFactorActive;
    private State<Boolean> isUtilVarAlive;

    private Map<Context, Node> cache;
    private long nodeCounter;
    Node terminal;

    private static double NO_BOUND = -Double.MAX_VALUE;

    public AndOrSearch(StateManager sm, IntVar v, Factor[] factors, Supplier<VariableBranches> branching) {
        this.sm = sm;
        this.cp = v.getSolver();
        this.branching = branching;
        this.factors = factors;
        objectiveVar = v;
        this.performCaching = false;
        this.performBounding = false;

        int numFactors = factors.length;
        isFactorActive = new ArrayList<State<Boolean> >(numFactors);
        for (int i = 0; i < numFactors; i++)
            isFactorActive.add(sm.makeStateRef(true));
        isUtilVarAlive = sm.makeStateRef(true);

        cache = new HashMap<>();
        nodeCounter = 0L;
    }

    public void onSolution(Procedure listener) {
        solutionListeners.add(listener);
    }

    public void onFailure(Procedure listener) {
        failureListeners.add(listener);
    }

    private void notifySolution() {
        solutionListeners.forEach(s -> s.call());
    }

    private void notifyFailure() {
        failureListeners.forEach(s -> s.call());
    }

    public Node solve() {
        return solve(true, true);
    }

    public Node solve(boolean performCaching) {
        return solve(performCaching, false);
    }

    public Node solve(boolean performCaching, boolean performBounding) {
        this.performCaching = performCaching;
        this.performBounding = performBounding;
        if (performCaching)
            terminal = createLeafNode(1);

        Node root = null;
        int level = sm.getLevel();
        sm.saveState();
        try {
            root = aos();
        } catch (StackOverflowError e) {
            throw new NotImplementedException("dfs with explicit stack needed to pass this test");
        }
        sm.restoreStateUntil(level);
        return root;
    }

    private Node aos() {
        return aos(NO_BOUND);
    }

    // TODO a stack instead of recursion
    private Node aos(double lowerBound) {

        VariableBranches branches = branching.get();

        if (branches.isEmpty()) {
            notifySolution();
            assert (objectiveVar.isFixed());
            if (performCaching)
                return terminal;
            else
                return createLeafNode(1.0);
        }

        Context context = null;
        if (performCaching) {
            context = cp.getContext();
            if (cache.containsKey(context))
                return cache.get(context);
        }

        IntVar branchingVariable = branches.getVariable();
        Node currentNode = createInternalNode(branchingVariable.getName(), branchingVariable.isDecision());

        double[] andUpperBounds = setupBounds(currentNode, lowerBound, branches);

        int[] branchValues = branches.getValues();
        for (int i = 0; i < branchValues.length; i++) {

            int branchingValue = branchValues[i];

            if (andNodeIsBounded(currentNode))
                throw InconsistencyException.INCONSISTENCY;

            int level = sm.getLevel();
            sm.saveState();

            Node childNode = null;
            double nodeCoefficient = 0;

            try {

                branchingVariable.fix(branchingValue, true);
                branchingVariable.getSolver().fixPoint();
                nodeCoefficient = findReducedFactors();

                if (orBranchIsBounded(currentNode, nodeCoefficient))
                    childNode = createBoundedNode();
                else
                    childNode =
                        aos(childLowerBound(currentNode, nodeCoefficient,
                                            andUpperBounds != null ? andUpperBounds[i] : NO_BOUND));

            } catch (InconsistencyException e) {
                notifyFailure();
                childNode = createFailureNode();
            }

            sm.restoreStateUntil(level);
            processChild(currentNode, childNode, branchingValue, nodeCoefficient);

        }

        if (!currentNode.hasValue())
            throw InconsistencyException.INCONSISTENCY;

        if (performCaching)
            cache.put(context, currentNode);

        return currentNode;

    }

    private double[] setupBounds(Node currentNode, double lowerBound, VariableBranches branches) {
        if (!performBounding || !isValidBound(lowerBound))
            return null;

        currentNode.setLowerBound(lowerBound);
        double[] andBranchesUB = null;
        if (currentNode.isRandom()) {
            andBranchesUB = andBranchesUpperBounds(branches);
            currentNode.setAndUb(andBranchesUB[andBranchesUB.length - 1]);
        }
        return andBranchesUB;
    }

    private boolean isValidBound(double bound) {
        return bound > NO_BOUND;
    }

    private double shallowUpperBound(double nodeCoefficient) {
        if (objectiveVar.isFixed())
            return nodeCoefficient;
        return nodeCoefficient*objectiveVar.max();
    }

    private double[] andBranchesUpperBounds(VariableBranches branches) {
        IntVar x = branches.getVariable();
        int[] branchValues = branches.getValues();
        double sum = 0.0;
        // Stores the sum of the upper bounds in the last element
        double[] ubs = new double[branchValues.length + 1];

        try {
            for (int i = 0; i < branchValues.length; i++) {
                int level = sm.getLevel();
                sm.saveState();

                x.fix(branchValues[i], true);
                x.getSolver().fixPoint();
                double nodeCoefficient = findReducedFactors();
                double ub = shallowUpperBound(nodeCoefficient);
                ubs[i] = ub;
                sum += ub;

                sm.restoreStateUntil(level);
            }
        } catch (InconsistencyException e) {
            // TODO(Linnea, 2023-09-21): handle failures in the root node
            assert(false);
        }

        ubs[branchValues.length] = sum;
        return ubs;
    }

    private boolean orBranchIsBounded(Node currentNode, double nodeCoefficient) {
        if (!performBounding || currentNode.isRandom() || !currentNode.hasLowerBound())
            return false;

        return currentNode.getLowerBound() >= shallowUpperBound(nodeCoefficient);
    }

    private boolean andNodeIsBounded(Node currentNode) {
        if (!performBounding || currentNode.isDecision() || !currentNode.hasLowerBound())
            return false;

        return currentNode.getLowerBound() >= currentNode.getAndUb();
    }

    private double childLowerBound(Node currentNode, double nodeCoefficient, double andChildUB) {
        if (!performBounding || !currentNode.hasLowerBound())
            return NO_BOUND;

        if (currentNode.isDecision()){
            return currentNode.getLowerBound() / nodeCoefficient;
        } else {
            assert(isValidBound(andChildUB));
            currentNode.reduceAndUbBy(andChildUB);
            return (currentNode.getLowerBound() - currentNode.getAndUb()) / nodeCoefficient;
        }
    }

    // TODO perform the probability multiplications with high-accuracy arithmetics
    private void processChild(Node parent, Node child, int branchingValue, double edgeValue) {

        parent.addChild(new Edge(parent, child, branchingValue, edgeValue));

        if (child.isBounded()) {
            assert(parent.isDecision());
            return;
        }

        if (child.isFailure()) {
            if (parent.isRandom())
                throw InconsistencyException.INCONSISTENCY;
            else
                return;
        }

        double childValue = child.getValue() * edgeValue;

        if (parent.isDecision()) {

            if (!parent.hasValue() || parent.getValue() < childValue) {
                parent.setValue(childValue);
                parent.setBestChild(child);
                if (performBounding)
                    parent.setLowerBound(child.getValue() * edgeValue);
            }

        } else {
            if (!parent.hasValue())
                parent.setValue(childValue);
            else
                parent.setValue(parent.getValue() + childValue);

            if (parent.hasLowerBound()) {
                assert(performBounding);
                parent.setLowerBound(parent.getLowerBound() - child.getValue() * edgeValue);
            }
        }
    }

    // TODO Use a trail-based method to check factors
    private double findReducedFactors() {
        double coefficient = 1;
        if (isUtilVarAlive.value() && objectiveVar.isFixed()) {
            isUtilVarAlive.setValue(false);
            coefficient *= objectiveVar.max();
        }
        for (int i = 0; i < factors.length; i++)
            if (isFactorActive.get(i).value() && !factors[i].isActive()) {
                isFactorActive.get(i).setValue(false);
                coefficient *= factors[i].getProbability();
            }
        return coefficient;
    }

    private Node createInternalNode(String name, boolean isDecision) {
        Node node = new Node(nodeCounter++);
        node.makeInternal(name, isDecision);
        return node;
    }

    private Node createFailureNode() {
        Node node = new Node(nodeCounter++);
        node.makeFailure();
        return node;
    }

    private Node createBoundedNode() {
        Node node = new Node(nodeCounter++);
        node.makeBounded();
        return node;
    }

    private Node createLeafNode(double value) {
        Node node = new Node(nodeCounter++);
        node.makeLeaf(value);
        return node;
    }

    public long getTreeSize() {
        return nodeCounter-1;
    }

}
