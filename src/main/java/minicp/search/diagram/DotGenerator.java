package minicp.search.diagram;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class DotGenerator {

    private class DotNode {
        String name;
        String shape;
        String label;
        String style;
        String color;

        public DotNode(String name) {
            this.name = name;
            this.shape = null;
            this.label = null;
            this.style = null;
            this.color = null;
        }
    }

    private class DotEdge {
        String first;
        String second;
        String style;
        String label;
        String color;

        public DotEdge(DotNode first, DotNode second, String label) {
            this.first = first.name;
            this.second = second.name;
            this.label = label;
            this.style = null;
            this.color = null;
        }
    }

    private Node root;
    private ArrayList<DotNode> nodes;
    private ArrayList<DotEdge> edges;
    private String dotStr;
    private DecimalFormat df;

    public DotGenerator(Node root) {
        this.root = root;
        nodes = new ArrayList<DotNode>();
        edges = new ArrayList<DotEdge>();
        df = new DecimalFormat("#.####");
        processGraph();
        createDotStr();
    }

    void createDotStr() {

        StringBuilder str = new StringBuilder();
        str.append("digraph G {\n");

        for (DotNode node : nodes) {
            String nodeStr = " " + node.name + " ";

            if (node.label != null || node.shape != null || node.style != null || node.color != null) {
                nodeStr += "[";
                boolean empty = true;

                if (node.label != null) {
                    nodeStr += "label=\"" + node.label + "\"";
                    empty = false;
                }

                if (node.shape != null) {
                    if (!empty)
                        nodeStr += ",";
                    else
                        empty = false;
                    nodeStr += "shape=" + node.shape;
                }

                if (node.style != null) {
                    if (!empty)
                        nodeStr += ",";
                    else
                        empty = false;
                    nodeStr += "style=" + node.style;
                }

                if (node.color != null) {
                    if (!empty)
                        nodeStr += ",";
                    else
                        empty = false;
                    nodeStr += "color=" + node.color;
                }

                nodeStr += "]";
            }

            nodeStr += ";\n";
            str.append(nodeStr);
        }

        for (DotEdge edge : edges) {
            String edgeStr = edge.first + " -> " + edge.second + " ";

            if (edge.label != null || edge.style != null || edge.color != null) {
                edgeStr += "[";
                boolean empty = true;

                if (edge.label != null) {
                    edgeStr += "label=\"" + edge.label + "\"";
                    empty = false;
                }

                if (edge.style != null) {
                    if (!empty)
                        edgeStr += ",";
                    else
                        empty = false;
                    edgeStr += "style=" + edge.style;
                }

                if (edge.color != null) {
                    if (!empty)
                        edgeStr += ",";
                    else
                        empty = false;
                    edgeStr += "color=" + edge.color;
                }

                edgeStr += "]";
            }

            edgeStr += ";\n";
            str.append(edgeStr);
        }

        str.append("}\n");
        dotStr = str.toString();
    }

    public String getDotStr() {
        return dotStr;
    }

    void processGraph() {

        HashMap<Long, DotNode> nodeMap = new HashMap<Long, DotNode>();
        Stack<Node> graphNodes = new Stack<Node>();
        Set<Long> visited = new HashSet<>();

        graphNodes.push(root);
        visited.clear();
        visited.add(root.getId());
        while (!graphNodes.empty()) {
            Node currentNode = graphNodes.pop();
            DotNode dotNode = new DotNode("n" + currentNode.getId());
            nodeMap.put(currentNode.getId(), dotNode);
            for (Edge e : currentNode.getEdges()) {
                Node child = e.getTarget();
                if (!visited.contains(child.getId())) {
                    graphNodes.push(child);
                    visited.add(child.getId());
                }
            }
        }

        graphNodes.push(root);
        visited.clear();
        visited.add(root.getId());
        while (!graphNodes.empty()) {

            Node currentNode = graphNodes.pop();
            DotNode dotNode = nodeMap.get(currentNode.getId());

            if (currentNode.isFailure()) {

                dotNode.label = "";
                dotNode.style = "filled";
                dotNode.color = "red";
                dotNode.shape = "diamond";

            } else if (currentNode.isLeaf()) {

                dotNode.label = df.format(currentNode.getValue());
                dotNode.shape = "octagon";

            } else {

                // a choice node (random or decision)

                dotNode.label = currentNode.getName() + "\\n" + df.format(currentNode.getValue());

                if (currentNode.isFailure()) {

                    dotNode.style = "filled";
                    dotNode.color = "coral";
                }

                if (currentNode.isDecision())
                    dotNode.shape = "box";
                else
                    dotNode.shape = "ellipse";

                for (Edge e : currentNode.getEdges()) {
                    Node child = e.getTarget();
                    if (!visited.contains(child.getId())) {
                        graphNodes.push(child);
                        visited.add(child.getId());
                    }
                    DotNode childDotNode = nodeMap.get(child.getId());
                    String edgeLabel = Integer.toString(e.getVariableValue());
                    if (!child.isFailure())
                        edgeLabel += "\\n" + df.format(e.getWeight());
                    DotEdge dotEdge = new DotEdge(dotNode, childDotNode, edgeLabel);
                    if (!currentNode.isDecision() || currentNode.getBestChild() == child)
                        dotEdge.style = "bold";
                    else
                        dotEdge.style = "bold";
                    edges.add(dotEdge);
                }

            }

            nodes.add(dotNode);

        }
    }

}
