package minicp.search.diagram;

public class Edge {

    private Node source;
    private Node target;
    int variableValue;
    private double weight;

    public Edge(Node source, Node target, int variableValue, double weight) {
        this.source = source;
        this.target = target;
        this.weight = weight;
        this.variableValue = variableValue;
    }

    public Node getSource() {
        return source;
    }

    public Node getTarget() {
        return target;
    }

    public double getWeight() {
        return weight;
    }

    public int getVariableValue() {
        return variableValue;
    }

}
