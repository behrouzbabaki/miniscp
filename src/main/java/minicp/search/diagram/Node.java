package minicp.search.diagram;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

public class Node {

    private long id;
    private String variableName;
    private boolean isDecision;
    private boolean isFailure;
    private boolean isBounded;
    private boolean hasLowerBound;
    private double lowerBound;
    private double andUb;
    private boolean hasValue;
    private double value;
    private boolean isLeaf;
    private Node bestChild;
    private ArrayList<Edge> outgoing;
    private ArrayList<Node> parents;
    private BigInteger subgraphSize;

    public Node(long id) {
        this.id = id;
        isLeaf = false;
        isFailure = false;
        isBounded = false;
        hasValue = false;
        hasLowerBound = false;
        outgoing = new ArrayList<Edge>();
        parents = new ArrayList<Node>();
        subgraphSize = BigInteger.valueOf(-1);
    }

    public void makeFailure() {
        isFailure = true;
        this.subgraphSize = BigInteger.ZERO;
    }

    public void makeBounded() {
        isBounded = true;
    }

    public void makeLeaf(double value) {
        isLeaf = true;
        this.value = value;
        this.subgraphSize = BigInteger.ZERO;
    }

    public void makeInternal(String variableName, boolean isDecision) {
        this.variableName = variableName;
        this.isDecision = isDecision;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        hasValue = true;
        this.value = value;
    }

    public boolean hasValue() {
        return hasValue;
    }

    public void addChild(Edge e) {
        outgoing.add(e);
        e.getTarget().parents.add(this);
    }

    public String getName() {
        return variableName;
    }

    public boolean isDecision() {
        return isDecision;
    }

    public boolean isRandom() {
        return !isDecision;
    }

    public boolean isBounded() {
        return isBounded;
    }

    public boolean hasLowerBound() {
        return hasLowerBound;
    }

    public double getLowerBound() {
        assert(hasLowerBound);
        return lowerBound;
    }

    public void setLowerBound(double lb) {
        hasLowerBound = true;
        lowerBound = lb;
    }

    public void setAndUb(double ub) {
        andUb = ub;
    }

    public double getAndUb() {
        assert(isRandom());
        return andUb;
    }

    public void reduceAndUbBy(double delta) {
        andUb -= delta;
    }

    public boolean isFailure() {
        return isFailure;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public long getId() {
        return this.id;
    }

    public void setBestChild(Node child) {
        this.bestChild = child;
    }

    public Node getBestChild() {
        return bestChild;
    }

    public ArrayList<Edge> getEdges() {
        return outgoing;
    }

    public ArrayList<Node> parents() {
        return parents;
    }

    @Override
    public boolean equals(Object obj) {
        return this.id == ((Node) obj).getId();
    }

    public void toDot(String filename) {

        PrintWriter writer = null;

        try {

            writer = new PrintWriter(filename);
            DotGenerator dotter = new DotGenerator(this);
            String dotStr = dotter.getDotStr();
            writer.print(dotStr);
            writer.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public BigInteger getSubgraphSize() {
        if (subgraphSize.compareTo(BigInteger.ZERO) >= 0)
            return subgraphSize;

        subgraphSize = BigInteger.ZERO;
        for (Edge e : outgoing) {
            subgraphSize = subgraphSize.add(BigInteger.ONE);
            subgraphSize = subgraphSize.add(e.getTarget().getSubgraphSize());
        }

        return subgraphSize;
    }

}
